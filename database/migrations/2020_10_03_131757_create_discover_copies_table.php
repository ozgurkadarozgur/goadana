<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscoverCopiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discover_copies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->string('icon')->nullable();
            $table->string('image_url')->nullable();
            $table->timestamps();
        });

        Schema::table('discovers', function (Blueprint $table) {
           $table->unsignedTinyInteger('copy_id')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discover_copies');
        Schema::table('discovers', function (Blueprint $table) {
            $table->dropColumn('copy_id');
        });
    }
}
