<?php

use Illuminate\Database\Seeder;
use App\Models\DiscoverCopy;
class DiscoverCopySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (DiscoverCopy::ITEMS as $item) {
            DiscoverCopy::create($item);
        }
    }
}
