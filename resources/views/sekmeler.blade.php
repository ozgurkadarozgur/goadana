<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="anasayfa" role="tabpanel" aria-labelledby="anasayfa-tab">


        <section>
            <!--<div class="tab-1-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#badc58;right:40px;top:60px"></div>-->
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpsec">
                                <div class="planla">
                                    <h2>PLANLA</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website_v2/images/resource/hps1.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Feke</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website_v2/images/resource/hpsb1.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Tufanbeyli</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website_v2/images/resource/hpsb2.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Çukurova</a></h3></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kesfet">
                                    <h2>KEŞFET</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website_v2/images/resource/hps2.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Kozan</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website_v2/images/resource/hpsb3.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Varda Köprüsü</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website_v2/images/resource/hpsb4.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Pozantı</a></h3></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

    <div class="tab-pane fade" id="planla" role="tabpanel" aria-labelledby="planla-tab">


        <section>
            <div class="tab-1-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#badc58;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="hpsec">
                                <div class="row">
                                    @foreach($plans as $plan)
                                        <div class="{{ $loop->iteration == 1 ? 'col-lg-6' : 'col-lg-3' }}">
                                            <div class="cities still">
                                                <a href="#" onclick="showPlanModal(this.parentNode)" title="asd"><img class="img-fluid img-resizing" src="{{ $plan->image_url }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">{{ $plan->title }}</a></h3></div>
                                                <div class="plan-items-card" style="display: none;">
                                                    <div class="plan-items-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                                <div class="hpsec">
                                                                    <div class="row">
                                                                        @foreach($plan->items as $plan_item)
                                                                            <div class="col-lg-3">
                                                                                <div class="cities still">
                                                                                    <a href="#" title=""><img class="img-resizing" src="{{ $plan_item->image_url }}" alt=""></a>
                                                                                    <div class="cities-title"><h3><a href="#" title="">{{ $plan_item->title }}</a></h3></div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <div class="tab-pane fade" id="kesfet" role="tabpanel" aria-labelledby="kesfet-tab">

        <section>
            <div class="tab-2-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#95afc0;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="hpsec">
                                <div class="row">
                                    @foreach($discovers as $discover)
                                        <div class="{{ $loop->iteration == 1 ? 'col-lg-6' : 'col-lg-3' }}">
                                            <div class="cities still">
                                                <a href="#" onclick="showPlanModal(this.parentNode)" title="asd"><img class="img-fluid img-resizing" src="{{ $discover->image_url }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">{{ $discover->title }}</a></h3></div>
                                                <div class="plan-items-card" style="display: none;">
                                                    <div class="plan-items-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                                <div class="hpsec">
                                                                    <div class="row">
                                                                        @foreach($discover->items as $discover_item)
                                                                            <div class="col-lg-3">
                                                                                <div class="cities still">
                                                                                    <a href="#" title=""><img class="img-resizing" src="{{ $discover_item->image_url }}" alt=""></a>
                                                                                    <div class="cities-title"><h3><a href="#" title="">{{ $discover_item->title }}</a></h3></div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <div class="tab-pane fade" id="afiyetle" role="tabpanel"  aria-labelledby="afiyetle-tab">



        <div class="hightlighticons button-group filters-button-group" style="background-color:#2c3e50" id="Afiyetle-Filtrele">
            <ul class="list-inline mb-0">
                <!-- For filtering controls add
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                <li class="list-inline-item" data-filter=".kebap"><a><i class="fal fa-wine-glass-alt"></i><span>Kebap</span></a> </li>
                <li class="list-inline-item" data-filter=".fastfood"><a><i class="fal fa-wine-glass-alt"></i><span>Fast Food</span></a> </li>
                <li class="list-inline-item" data-filter=".deneme"><a><i class="fal fa-wine-glass-alt"></i><span>Deneme</span></a> </li>
                -->
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                @foreach($place_types as $place_type)
                    <li class="list-inline-item" data-filter=".{{ Str::slug($place_type->title, '-') }}"><a><i class="fal fa-wine-glass-alt"></i><span>{{ $place_type->title }}</span></a> </li>
                @endforeach
            </ul>
        </div>






        <section>
            <div class="tab-3-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#2c3e50;right:40px;top:60px"></div>
            <div class="blocks">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="alert alert-dark" role="alert">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control quicksearch" placeholder="İsim veya ilçe ile arayın...">
                                    </div>

                                    <div class="col-lg-3 offset-lg-3 text-right">

                                        <a class="btn btn-secondary liste-gorunum-afiyetle" data-toggle="tooltip" data-placement="top" title="Liste Görünümü" style="color:#fff"><i class="fas fa-th-list"></i></a>
                                        <a class="btn btn-secondary harita-gorunum-afiyetle" data-toggle="tooltip" data-placement="top" title="Harita Görünümü" style="color:#fff"><i class="fal fa-map-marked"></i></a>
                                    </div>
                                </div>

                            </div>

                            <div id="liste-gorunum-afiyetle">
                                <div class="grid">
                                    <div class="row filter-container" style="position:relative">

                                        @foreach($places as $place)
                                            @php
                                                $category = Str::slug($place->place_type_matches->first()->place_type->title, '-')
                                            @endphp
                                            <div class="col-lg-3 element-item {{ $place->slug_type_matches() }}" data-category="{{ $place->slug_type_matches() }}">
                                                <div class="card mb-5">
                                                    <img class="img-resizing" src="{{ $place->image_url }}" alt="sample" />
                                                    <div class="card-body">
                                                        {{ $place->title }}
                                                        <br>
                                                        <small>Çukurova</small>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>


                            <div id="harita-gorunum-afiyetle" style="display:none;">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3186.7631800898125!2d35.33203111503039!3d36.9915856646031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15288f6fae5158d7%3A0x6aeaffe69e65a068!2sSabanc%C4%B1%20Merkez%20Camii!5e0!3m2!1str!2str!4v1584099409284!5m2!1str!2str" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>


    <div class="tab-pane fade" id="anla" role="tabpanel" aria-labelledby="anla-tab">

        <section>
            <div class="tab-4-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#3498db;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="min-height:600px">

                            <!-- Anla Modal -->
                            <div class="modal fade" id="anlaBlogDetay" tabindex="-1" role="dialog" aria-labelledby="anlaModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="anlaModalLabel">Blog Başlık</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lectus nulla, venenatis nec dui in, scelerisque suscipit mauris. Cras at euismod quam, eu accumsan nunc.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light" data-dismiss="modal">Kapat</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                @foreach($get_its as $get_it)
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="blog-wrapper home-blog-wrapper white-bg">
                                            <div class="meta-info">
                                                <ul class="mb-0">
                                                    <li class="posts-time">{{ $get_it->created_at->isoFormat('LL') }}</li>
                                                </ul>
                                            </div>
                                            <div class="blog-content home-blog">
                                                <img src="{{ $get_it->image_url }}" class="img-fluid img-resizing">
                                                <h2 class="blog-title">{{ $get_it->title }}</h2>
                                            </div>
                                            <div class="link-box home-blog-link">
                                                <a href="#" data-toggle="modal" data-target="#anlaBlogDetay">Ayrıntılar</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach



                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

    <div class="tab-pane fade" id="etkinlik" role="tabpanel" aria-labelledby="etkinlik-tab">

        <div class="hightlighticons button-group filters-button-group" style="background-color:#2c3e50" id="Afiyetle-Filtrele">
            <ul class="list-inline mb-0">
                <!-- For filtering controls add
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                <li class="list-inline-item" data-filter=".kebapciler"><a><i class="fal fa-wine-glass-alt"></i><span>Kebapçılar</span></a> </li>
                <li class="list-inline-item" data-filter=".meyhaneler"><a><i class="fal fa-wine-glass-alt"></i><span>Meyhaneler</span></a> </li>
                <li class="list-inline-item" data-filter=".kahvalti"><a><i class="fal fa-wine-glass-alt"></i><span>Kahvaltı</span></a> </li>
                <li class="list-inline-item" data-filter=".fastfood"><a><i class="fal fa-wine-glass-alt"></i><span>Fast Foods</span></a> </li>
                <li class="list-inline-item" data-filter=".restaurant"><a><i class="fal fa-wine-glass-alt"></i><span>Restaurant</span></a> </li>
                <li class="list-inline-item" data-filter=".yerellezzetler"><a><i class="fal fa-wine-glass-alt"></i><span>Yerel Lezzetler</span></a> </li>
                -->
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                @foreach($event_types as $event_type)
                    <li class="list-inline-item" data-filter=".{{ Str::slug($event_type->title, '-') }}"><a><i class="fal fa-wine-glass-alt"></i><span>{{ $event_type->title }}</span></a> </li>
                @endforeach
            </ul>
        </div>



        <section>
            <div class="tab-5-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#30d5c7;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="min-height:600px">



                            <div class="alert alert-dark" role="alert">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control quicksearch-event" placeholder="Etkinlik adı veya ücret durumunu ile ara...">
                                    </div>

                                    <div class="col-lg-3">
                                        <select class="custom-select">
                                            <option selected>Sıralama</option>
                                            <option value="1">Tarihe Göre En Yakın</option>
                                            <option value="1">Tarihe Göre En Uzak</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 text-right">

                                        <a class="btn btn-secondary liste-gorunum-etkinlik" data-toggle="tooltip" data-placement="top" title="Liste Görünümü" style="color:#fff"><i class="fas fa-th-list"></i></a>
                                        <a class="btn btn-secondary harita-gorunum-etkinlik" data-toggle="tooltip" data-placement="top" title="Harita Görünümü" style="color:#fff"><i class="fal fa-map-marked"></i></a>
                                    </div>
                                </div>

                            </div>


                            <div id="liste-gorunum-etkinlik">
                                <div class="grid-etkinlik">
                                    <div class="row filter-container" style="position:relative">

                                        @foreach($events as $event)
                                            <div class="col-lg-3 element-item {{ Str::slug($event->event_type->title, '-') }}" data-category="{{ Str::slug($event->event_type->title, '-') }}">

                                                <div class="card mb-5">
                                                    <img class="img-fluid img-resizing" src="{{ $event->image_url }}" alt="sample" />
                                                    <div class="card-body">
                                                        {{ $event->title }}
                                                        <br>
                                                        <small>Çukurova</small>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div id="harita-gorunum-etkinlik" style="display:none;">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3186.7631800898125!2d35.33203111503039!3d36.9915856646031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15288f6fae5158d7%3A0x6aeaffe69e65a068!2sSabanc%C4%B1%20Merkez%20Camii!5e0!3m2!1str!2str!4v1584099409284!5m2!1str!2str" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

    <div class="tab-pane fade" id="harita" role="tabpanel" aria-labelledby="harita-tab">
        <section>
            <div class="tab-6-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#fa9634;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="min-height:600px">
                            <iframe src="{{ asset('website_v2/gmap.html') }}" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</div>
