@inject('discoverCopies', 'App\Services\DiscoverCopyService')
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Rota Adana</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!-- Styles -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('website_v2/css/font-awesome/5.10.2/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('website_v2/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website_v2/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('website_v2/css/responsive.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <style>
        .tab-1:hover{background-color:#badc58!important;color:#000!important}
        .tab-2:hover{background-color:#95afc0!important;color:#fff!important}
        .tab-2:hover i{color:#fff!important}
        .tab-3:hover{background-color:#2c3e50!important;color:#fff!important}
        .tab-3:hover i{color:#fff!important}
        .tab-4:hover{background-color:#3498db!important;color:#fff!important}
        .tab-4:hover i{color:#fff!important}
        .tab-5:hover{background-color:#30d5c7!important;color:#fff!important}
        .tab-5:hover i{color:#fff!important}
        .tab-6:hover{background-color:#fa9634!important;color:#000!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-1{background-color:#badc58!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-2{background-color:#95afc0!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-3{background-color:#2c3e50!important;color:#fff}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-3 i{color:#fff!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-4{background-color:#3498db!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-5{background-color:#30d5c7!important;color:#fff}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-5 i{color:#fff!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-6{background-color:#fa9634!important}
        .planla h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;top:195px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .kesfet h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;top:480px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .afiyetle h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;top:770px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .anla h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;top:1050px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .etkinlik h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;bottom:25px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .filtr-item{margin-bottom:40px!important}
        .blog-wrapper{background:#fff;overflow:hidden;padding:20px;box-shadow:3px 1px 10px rgba(0,0,0,.12)}
        .blog-wrapper p{font-size:14px;line-height:26px}
        .home-blog-wrapper{transition:.3s}
        .blog-wrapper.home-blog-wrapper:hover{box-shadow:3px 1px 10px rgba(0,0,0,.52)}
        .blog-title{display:inline-block;font-size:30px;font-weight:600;line-height:1.4;margin:10px 0 15px;padding:0;text-align:left}
        .blog-content.home-blog h2{font-size:18px;margin-bottom:10px}
        .blog-content.home-blog p{margin-bottom:10px;text-align:justify}
        .link-box.home-blog-link a{font-size:12px;color:#c2cdeb;font-weight:500}
        .blog-title a{color:#555}
        .link-box a{color:#555}
        .link-box a:hover{color:#1c4899}
        .meta-info ul li{color:#9e9e9e;display:inline-block;font-size:11px;padding:0 12px;position:relative;text-transform:uppercase;font-weight:600}
        .embed-responsive{margin-bottom:20px}
        .meta-info ul li:first-child{padding-left:0}
        .meta-info ul li a{color:#ff5e14}
        .meta-info ul li a:hover{color:#444}
        .meta-info ul li::before{border:1px solid #999;border-radius:5px;content:"";height:5px;left:-4px;position:absolute;top:6px;width:5px}
        .meta-info ul li:first-child:before{display:none}
        .blog-thumb img{width:100%}

        #map{display:block;height:600px!important;}
    </style>

</head>
<body class="full-height" id="scrollup">

<div class="page-loading">
    <img src="{{ asset('website_v2/images/loader.gif') }}" alt="" />
    <span>Yükleniyor...</span>
</div>

<div class="theme-layout">

    <section>
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ asset('website_v2/images/resource/slider-bg.jpg') }}) repeat scroll 50% 422.28px transparent;" class="no-parallax parallax scrolly-invisible"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-lg-10">
                        <div class="listingsf lowmargin">
                            <h2 class="text-center mx-auto center-block">
                                <a href="/">
                                    <img src="{{ asset('website_v2/images/logo2.png') }}" style="float:none!important" class="img-fluid">
                                </a>
                            </h2>
                            <h3 style="text-shadow: 1px 1px 2px rgba(0,0,0,0.8)">Şehirlerin <span style="font-size:50px">01</span> Numarasını Keşfedin...</h3>
                            <p style="text-shadow: 1px 1px 2px rgba(0,0,0,0.8)">Şehrin uzmanlarından; konaklamak, yemek yemek, alışveriş yapmak veya ziyaret etmek için harika yerler bulun.</p>

                            <div class="browsehighlights">
                                <span>veya Kategorileri İncele</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white mt-5" id="sekmeler">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link tab-1" id="planla-tab" href="{{ route('web.planla') }}" aria-controls="planla" aria-selected="true"><span style="color:#c3dede;font-size:30px;margin-right:10px">01</span> <i class="fal fa-edit" style="color:#000;font-size:30px"></i> Planla</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-2" id="kesfet-tab" href="{{ route('web.kesfet') }}" aria-controls="kesfet" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">02</span> <i class="fab fa-sistrix" style="color:#000;font-size:30px"></i> Keşfet</a>
                                </li>
                                <li class="nav-item afiyetle">
                                    <a class="nav-link tab-3" id="afiyetle-tab" href="{{ route('web.afiyetle') }}" aria-controls="afiyetle" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">03</span> <i class="fal fa-utensils-alt" style="color:#000;font-size:30px"></i> Afiyetle</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-4" id="anla-tab" href="{{ route('web.anla') }}" aria-controls="anla" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">04</span> <i class="fal fa-map-signs" style="color:#000;font-size:30px"></i> Anla</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-5" id="etkinlik-tab" href="{{ route('web.etkinlikler') }}" aria-controls="etkinlik" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">05</span> <i class="fal fa-calendar-alt" style="color:#000;font-size:30px"></i> Etkinlikler</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-6" id="harita-tab" href="{{ route('web.harita') }}" aria-controls="harita" aria-selected="false" style="border:none"><span style="color:#c3dede;font-size:30px;margin-right:10px">06</span> <i class="fal fa-map-marked-alt" style="color:#000;font-size:30px"></i> Haritada Gör</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                @foreach($discoverCopies->items() as $item)
                                    <li class="nav-item">
                                        <a class="nav-link tab-1" id="kesfet-tab-{{ $item->id }}" href="{{ route('web.kesfet', [$item->id, $item->slug]) }}" aria-controls="kesfet" aria-selected="true">
                                            <span style="color:#c3dede;font-size:30px;margin-right:10px">{{ $loop->iteration + 6 }}</span>
                                            <i class="fal fa-edit" style="color:#000;font-size:30px"></i>
                                            {{ $item->title }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>

    <!-- tabs -->
    @yield('content')

    <footer class="dfooter">
        <div class="block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 column">
                        <div class="fabout">
                            <a href="#" title=""><img src="{{ asset('website_v2/images/logo2.png') }}" alt="" /></a>
                            <span>Seyhan, Adana</span>
                            <span>+90 322 111 22 33</span>
                        </div>
                    </div>
                    <div class="col-lg-2 offset-lg-1 column">
                        <h3 class="ftitle">Ne Yapıyoruz?</h3>
                        <div class="flink">
                            <a href="#" title="">Planla</a>
                            <a href="#" title="">Keşfet</a>
                            <a href="#" title="">Anla</a>
                            <a href="#" title="">Haritada Gör</a>
                            <a href="#" title="">İletişim</a>
                        </div>
                    </div>
                    <div class="col-lg-3 column">
                        <h3 class="ftitle">Mekanlar</h3>
                        <div class="flink">
                            <a href="#" title="">Kahvaltı</a>
                            <a href="#" title="">Restorant</a>
                            <a href="#" title="">Fast Foods</a>
                            <a href="#" title="">Kebapçılar</a>
                            <a href="#" title="">Yöresel Lezzet</a>
                        </div>
                    </div>
                    <div class="col-lg-3 column">
                        <h3 class="ftitle">Uygulamamızı İndirin</h3>
                        <div class="dbuttons">
                            <a href="#" title=""><img src="{{ asset('website_v2/images/ios.png') }}" alt="" /></a>
                            <a href="#" title=""><img src="{{ asset('website_v2/images/google.png') }}" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottomline s2">
            <a href="#scrollup" class="scrollup s2" title=""><i class="fa fa-angle-up"></i></a>
            <div class="container">
                <span>© 2020 RotaAdana | Her hakkı saklıdır.</span>
                <div class="social">
                    <a href="#" title=""><i class="fab fa-facebook"></i></a>
                    <a href="#" title=""><i class="fab fa-twitter"></i></a>
                    <a href="#" title=""><i class="fab fa-linkedin"></i></a>
                    <a href="#" title=""><i class="fab fa-pinterest"></i></a>
                    <a href="#" title=""><i class="fab fa-google"></i></a>
                    <a href="#" title=""><i class="fab fa-github"></i></a>
                </div>
            </div>
        </div>
    </footer>

</div>



<div class="clearfix"></div>
<!-- Modal -->
<div class="modal fade" id="ModalEkrani" tabindex="-1" role="dialog" aria-labelledby="GenelModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body" style="padding:0">
                <nav style="float:none">
                    <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" style="border-bottom:1px solid #dee2e6;border-right:1px solid #dee2e6" id="nav-icerikmodal-tab" data-toggle="tab" href="#nav-icerikmodal" role="tab" aria-controls="nav-home" aria-selected="true">İncele</a>
                        <a class="nav-item nav-link"  style="border-bottom:1px solid #dee2e6;border-right:none" id="nav-haritamodal-tab" data-toggle="tab" href="#nav-haritamodal" role="tab" aria-controls="nav-profile" aria-selected="false">Haritada Gör</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-icerikmodal" role="tabpanel" aria-labelledby="nav-icerikmodal-tab">
                        <div style="padding:20px;margin-bottom:30px">
                            <h6 class="mb-1">Açıklama</h6>
                            <div class="ldesc">
                                <p>Seyyahların gözüyle Adana<br>
                                    Ticaret yollarının üzerinde yer almasından dolayı Adanada bir çok gezginin uğradığı bir yer olmuştur.
                                    El-Gazzi Arap seyyah 1500'lü yıllarda Adana'ya yaptığı ziyaretlerde, Seyhan nehrinin akışını, çöreklenmiş bir yılana benzetir.</p>
                            </div>
                            <div class="amenties mt-3 mb-4">
                                <span><i class="fal fa-ticket-alt"></i> Müze Kartı Geçerli</span>
                                <span><i class="fal fa-ticket-alt"></i> Rezervasyon Gerektirmez</span>
                                <span><i class="fal fa-car"></i> Açık Alan</span>
                                <span><i class="fal fa-bicycle"></i> Bisiklet Yolu</span>
                                <span><i class="fal fa-wheelchair"></i> Engelli Rampası</span>
                                <span><i class="fal fa-calendar-alt"></i> Rezervasyon</span>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-haritamodal" role="tabpanel" aria-labelledby="nav-haritamodal-tab">

                        <iframe src="{{ asset('website_v2/gmap.html') }}" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('website_v2/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('website_v2/js/isotope-docs.min.js?6') }}"></script>
<script src="{{ asset('website_v2/js/script.js') }}" type="text/javascript"></script>
@yield('scripts')
<script>

    function showDiscoverModal(item) {
        let items = item.querySelector('.discover-items-body');
        let callback = $('#PlanlaModalEkrani').modal('show');
        let modal_callback = callback[0];
        let content = modal_callback.querySelector('#discover-modal-content');
        content.innerHTML = items.innerHTML;
    }

    $(document).ready(function(){
// init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });

// init Isotope
        var $gridetkinlik = $('.grid-etkinlik').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });
// filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function() {
                var number = $(this).find('.number').text();
                return parseInt( number, 10 ) > 50;
            },
            // show if name ends with -ium
            ium: function() {
                var name = $(this).find('.name').text();
                return name.match( /ium$/ );
            }
        };


// bind filter button click
        $('.filters-button-group').on( 'click', 'li', function() {
            var filterValue = $( this ).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[ filterValue ] || filterValue;
            $grid.isotope({ filter: filterValue });
            $gridetkinlik.isotope({ filter: filterValue });
        });
// change is-checked class on buttons
        $('.button-group').each( function( i, buttonGroup ) {
            var $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'li', function() {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $( this ).addClass('is-checked');
            });
        });


// quick search regex
        var qsRegex;

// use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            var $grid = $('.grid').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows',
                filter: function() {
                    return qsRegex ? $(this).text().match( qsRegex ) : true;
                }
            });
            var $gridetkinlik = $('.grid-etkinlik').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows',
                filter: function() {
                    return qsRegex ? $(this).text().match( qsRegex ) : true;
                }
            });
            $grid.isotope();
            $gridetkinlik.isotope();

        }, 200 ) );

        var $quicksearchEvent = $('.quicksearch-event').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearchEvent.val(), 'gi' );
            var $grid = $('.grid-etkinlik').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows',
                filter: function() {
                    return qsRegex ? $(this).text().match( qsRegex ) : true;
                }
            });
            var $gridetkinlik = $('.grid-etkinlik').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows',
                filter: function() {
                    return qsRegex ? $(this).text().match( qsRegex ) : true;
                }
            });
            $grid.isotope();
            $gridetkinlik.isotope();

        }, 200 ) );

// debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            threshold = threshold || 100;
            return function debounced() {
                clearTimeout( timeout );
                var args = arguments;
                var _this = this;
                function delayed() {
                    fn.apply( _this, args );
                }
                timeout = setTimeout( delayed, threshold );
            };
        }

        $('.nav-tabs a').on('shown.bs.tab', function(){
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#sekmeler").offset().top
            }, 200);
            $grid.isotope({ filter: '*' });
            $gridetkinlik.isotope({ filter: '*' });
        });








        $(".harita-gorunum-etkinlik").click(function() {
            $("#liste-gorunum-etkinlik").hide();
            $("#harita-gorunum-etkinlik").show();
        });

        $(".liste-gorunum-etkinlik").click(function() {
            $("#liste-gorunum-etkinlik").show();
            $("#harita-gorunum-etkinlik").hide();
        });
        $(".harita-gorunum-afiyetle").click(function() {
            $("#liste-gorunum-afiyetle").hide();
            $("#harita-gorunum-afiyetle").show();
        });

        $(".liste-gorunum-afiyetle").click(function() {
            $("#liste-gorunum-afiyetle").show();
            $("#harita-gorunum-afiyetle").hide();
        });


    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
</body>
</html>
