@extends('layouts.app')

@section('content')

    <div class="container">
        <div id="map" style="width: 100%; height: 500px;"></div>
    </div>

@endsection

@section('scripts')

    <script>
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFSPJrDhbTqLYU09d9qo8caSvhlcKeUQU&callback=initMap"
            async defer></script>
@endsection