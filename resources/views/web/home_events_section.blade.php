<div class="row">
    @foreach($event_types as $event_type)
        <div class="{{ $loop->iteration == 1 ? 'col-lg-6' : 'col-lg-3' }}">
            <div class="cities still">
                <a href="#" onclick="showPlanModal(this.parentNode, {{  $event_type->id }})" title="asd"><img class="img-fluid img-resizing" src="{{ $event_type->image_url }}" alt="" /></a>
                <div onclick="showPlanModal(this.parentNode, {{  $event_type->id }})" class="cities-title"><h3><a href="#" title="">{{ $event_type->title }}</a></h3></div>
                <div class="plan-items-card" style="display: none;">
                    <div class="plan-items-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="hpsec">
                                    <div class="row">
                                        @foreach($event_type->events as $event)
                                            <div class="col-lg-3">
                                                <div class="cities still">
                                                    <a onclick="showPlanDetail(this.parentNode)" href="#" title=""><img class="img-resizing" src="{{ $event->image_url }}" alt=""></a>
                                                    <div class="cities-title"><h3><a href="#" title="">{{ $event->title }}</a></h3></div>
                                                    <div class="plan-item-detail-card" style="display: none">
                                                        <div class="plan-item-detail-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-light" onclick="backPlanModal()">Geri</a>
                                                                    <div class="card mb-3">
                                                                        <img class="card-img-top img-fluid img-resizing" src="{{ $event->image_url }}" alt="Card image cap">
                                                                        <div class="card-body">
                                                                            <h5 class="card-title">{{ $event->title }}</h5>
                                                                            <p class="card-text">{!! $event->text !!}</p>
                                                                            <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $event->created_at->format('d-m-Y') }}</small></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>