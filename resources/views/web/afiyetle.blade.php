@extends('layouts.web')

@section('content')

    <div class="tab-pane" id="afiyetle" role="tabpanel"  aria-labelledby="afiyetle-tab">

        <!-- Anla Modal -->
        <div class="modal fade" id="anlaBlogDetay" tabindex="-1" role="dialog" aria-labelledby="anlaModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lectus nulla, venenatis nec dui in, scelerisque suscipit mauris. Cras at euismod quam, eu accumsan nunc.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Kapat</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="hightlighticons button-group filters-button-group" style="background-color:#2c3e50" id="Afiyetle-Filtrele">
            <ul class="list-inline mb-0">
                <!-- For filtering controls add
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                <li class="list-inline-item" data-filter=".kebap"><a><i class="fal fa-wine-glass-alt"></i><span>Kebap</span></a> </li>
                <li class="list-inline-item" data-filter=".fastfood"><a><i class="fal fa-wine-glass-alt"></i><span>Fast Food</span></a> </li>
                <li class="list-inline-item" data-filter=".deneme"><a><i class="fal fa-wine-glass-alt"></i><span>Deneme</span></a> </li>
                -->


                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-compress-arrows-alt"></i><span>Hepsi</span></a> </li>
                @foreach($place_types as $place_type)
                    <li class="list-inline-item" data-filter=".{{ Str::slug($place_type->title, '-') }}">
                        <a>
                            <i class="{{ $place_type->icon }}"></i>
                            <span>{{ $place_type->title }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>






        <section>
            <div class="tab-3-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#2c3e50;right:40px;top:60px"></div>
            <div class="blocks">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="alert alert-dark" role="alert">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control quicksearch" placeholder="İsim veya ilçe ile arayın...">
                                    </div>

                                    <div class="col-lg-3 offset-lg-3 text-right">

                                        <a class="btn btn-secondary liste-gorunum-afiyetle" data-toggle="tooltip" data-placement="top" title="Liste Görünümü" style="color:#fff"><i class="fas fa-th-list"></i></a>
                                        <a class="btn btn-secondary harita-gorunum-afiyetle" data-toggle="tooltip" data-placement="top" title="Harita Görünümü" style="color:#fff"><i class="fal fa-map-marked"></i></a>
                                    </div>
                                </div>

                            </div>

                            <div id="liste-gorunum-afiyetle">
                                <div class="grid">
                                    <div class="row filter-container" style="position:relative">

                                        @foreach($places as $place)
                                            @php
                                                $category = Str::slug($place->place_type_matches->first()->place_type->title, '-')
                                            @endphp
                                            <div class="col-lg-3 element-item {{ $place->slug_type_matches() }}" data-category="{{ $place->slug_type_matches() }}">
                                                <div class="card mb-4">
                                                    <img onclick="showPlanModal(this.parentNode)" title="{{ $place->title }}" style="cursor: pointer" class="img-resizing" src="{{ $place->image_url }}" alt="sample" />
                                                    <div class="card-body">
                                                        <span style="cursor: pointer;" onclick="showPlanModal(this.parentNode.parentNode.parentNode)">{{ $place->title }}</span>
                                                        <br>
                                                        <small>{{ Str::limit($place->address, 30) }}</small>
                                                    </div>
                                                    <div class="anla-content" style="display: none">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="card mb-3">
                                                                    <img class="card-img-top img-fluid img-resizing" src="{{ $place->image_url }}" alt="Card image cap">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">{{ $place->title }}</h5>
                                                                        <p class="card-text">{!! $place->description !!}</p>
                                                                        <p class="card-text">Adres: {{ $place->address ? $place->address : 'Belirtilmemiş' }}</p>
                                                                        <p class="card-text">Telefon: {{ $place->phone ? $place->phone : 'Belirtilmemiş' }}</p>
                                                                        <p class="card-text">Çalışma Saatleri: {{ $place->working_hours ? $place->working_hours : 'Belirtilmemiş' }}</p>
                                                                        <div class="amenties mt-3">
                                                                            @foreach($place->featureList as $feature)
                                                                                <span><img src="{{ $feature->icon_url }}" width="40" height="40" /> {{ $feature->title }}</span>
                                                                            @endforeach
                                                                        </div>
                                                                        <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $place->created_at->format('d-m-Y') }}</small></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>


                            <div id="harita-gorunum-afiyetle" style="display:none;">
                                <iframe src="" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection


@section('scripts')

    <script>

        $(document).ready(function() {

            let url = ('{{ asset('website_v2/gmap.html') }}');
            let location_uri = ('{{ route('api.place_type.locations') }}')

            $('iframe').attr('src', url + '?location_uri='+ location_uri)
        })

        function showPlanModal(item) {
            console.log(item)

            let items = item.querySelector('.anla-content');
            let callback = $('#anlaBlogDetay').modal('show');
            let modal_callback = callback[0];
            let content = modal_callback.querySelector('#anlaBlogDetay .modal-body');
            content.innerHTML = items.innerHTML;

        }

    </script>

@endsection