
<!-- Planla Modal -->
<div class="modal fade" id="PlanlaModalEkrani" tabindex="-1" role="dialog" aria-labelledby="PlanlaModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body" style="padding:0">
                <nav style="float:none">
                    <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                        <!--
                        <a onclick="backPlanModal()" class="nav-item nav-link" style="border-bottom:1px solid #dee2e6;border-right:1px solid #dee2e6" id="nav-back" data-toggle="tab" href="#" role="tab" aria-controls="nav-back" aria-selected="false">Geri</a>
                        -->
                        <a class="nav-item nav-link active" style="border-bottom:1px solid #dee2e6;border-right:1px solid #dee2e6" id="nav-planlamodal-tab" data-toggle="tab" href="#nav-planlamodal" role="tab" aria-controls="nav-home" aria-selected="true">İncele</a>
                        <a class="nav-item nav-link"  style="border-bottom:1px solid #dee2e6;border-right:none" id="nav-planlaharita-tab" data-toggle="tab" href="#nav-planlaharita" role="tab" aria-controls="nav-profile" aria-selected="false">Haritada Gör</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-planlamodal" role="tabpanel" aria-labelledby="nav-planlamodal-tab">
                        <div style="padding:20px;margin-bottom:30px">

                            <div id="plan-modal-content">
                                plan modal content
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-planlaharita" role="tabpanel" aria-labelledby="nav-planlaharita-tab-tab">

                        <iframe src="" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    @foreach($plans as $plan)
        <div class="{{ $loop->iteration == 1 ? 'col-lg-6' : 'col-lg-3' }}">
            <div class="cities still">
                <a href="#" onclick="showPlanModal(this.parentNode, {{  $plan->id }})" title="{{ $plan->title }}"><img class="img-fluid img-resizing" src="{{ $plan->image_url }}" alt="" /></a>
                <div onclick="showPlanModal(this.parentNode)" class="cities-title"><h3><a href="#" title="">{{ $plan->title }}</a></h3></div>
                <div class="plan-items-card" style="display: none;">
                    <div class="plan-items-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="hpsec">
                                    <div class="row">
                                        @foreach($plan->items as $plan_item)
                                            <div class="col-lg-3">
                                                <div class="cities still">
                                                    <a href="#" onclick="showPlanDetail(this.parentNode)" title=""><img class="img-resizing" title="{{ $plan_item->title }}" src="{{ $plan_item->image_url }}" alt="{{ $plan_item->title }}"></a>
                                                    <div class="cities-title"><h3><a href="#" title="">{{ $plan_item->title }}</a></h3></div>
                                                    <div class="plan-item-detail-card" style="display: none">
                                                        <div class="plan-item-detail-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-light" onclick="backPlanModal()">Geri</a>
                                                                    <div class="card mb-3">
                                                                        <img class="card-img-top img-fluid img-resizing" src="{{ $plan_item->image_url }}" alt="Card image cap">
                                                                        <div class="card-body">
                                                                            <h5 class="card-title">{{ $plan_item->title }}</h5>
                                                                            <p class="card-text">{!! $plan_item->text !!}</p>
                                                                            <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $plan_item->created_at->format('d-m-Y') }}</small></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>