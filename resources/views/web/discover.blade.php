@extends('layouts.web')

@section('content')

    <!-- Planla Modal -->
    <div class="modal fade" id="PlanlaModalEkrani" tabindex="-1" role="dialog" aria-labelledby="PlanlaModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body" style="padding:0">
                    <nav style="float:none">
                        <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" style="border-bottom:1px solid #dee2e6;border-right:1px solid #dee2e6" id="nav-planlamodal-tab" data-toggle="tab" href="#nav-planlamodal" role="tab" aria-controls="nav-home" aria-selected="true">İncele</a>
                            <a class="nav-item nav-link"  style="border-bottom:1px solid #dee2e6;border-right:none" id="nav-planlaharita-tab" data-toggle="tab" href="#nav-planlaharita" role="tab" aria-controls="nav-profile" aria-selected="false">Haritada Gör</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-planlamodal" role="tabpanel" aria-labelledby="nav-planlamodal-tab">
                            <div style="padding:20px;margin-bottom:30px">

                                <div id="plan-modal-content">
                                    plan modal content
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-planlaharita" role="tabpanel" aria-labelledby="nav-planlaharita-tab-tab">

                            <iframe class="mapframe" src="{{ asset('website_v2/gmap.html') }}" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="tab-pane" id="kesfet" role="tabpanel" aria-labelledby="kesfet-tab">

        <section>
            <div class="tab-2-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#95afc0;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="hpsec">
                                <div class="row">
                                    @foreach($discovers as $discover)
                                        <div class="{{ $loop->iteration == 1 ? 'col-lg-6' : 'col-lg-3' }}">
                                            <div class="cities still">
                                                <a href="#" onclick="showPlanModal(this.parentNode, {{  $discover->id }})" title="{{ $discover->title }}"><img class="img-fluid img-resizing" src="{{ $discover->image_url }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" onclick="showPlanModal(this.parentNode.parentNode.parentNode, {{  $discover->id }})" title="{{ $discover->title }}">{{ $discover->title }}</a></h3></div>
                                                <div class="plan-items-card" style="display: none;">
                                                    <div class="plan-items-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                                <div class="hpsec">
                                                                    <div class="row">
                                                                        @foreach($discover->items as $discover_item)
                                                                            <div class="col-lg-3">
                                                                                <div class="cities still">
                                                                                    <a onclick="showPlanDetail(this.parentNode)" href="#" title="{{ $discover_item->title }}"><img class="img-resizing" src="{{ $discover_item->image_url }}" alt=""></a>
                                                                                    <div class="cities-title"><h3><a href="#" onclick="showPlanDetail(this.parentNode.parentNode.parentNode)" title="{{ $discover_item->title }}">{{ $discover_item->title }}</a></h3></div>
                                                                                    <div class="plan-item-detail-card" style="display: none">
                                                                                        <div class="plan-item-detail-body">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <a href="#" class="btn btn-light" onclick="backPlanModal()">Geri</a>
                                                                                                        <div class="card mb-3">
                                                                                                                @if($discover_item->video_id)
                                                                                                                    <div class="embed-responsive embed-responsive-21by9">
                                                                                                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $discover_item->video_id }}?rel=0" allowfullscreen></iframe>
                                                                                                                    </div>
                                                                                                                    @else
                                                                                                                <img class="card-img-top img-fluid img-resizing" src="{{ $discover_item->image_url }}" alt="{{ $discover_item->title }}">
                                                                                                                @endif
                                                                                                                <div class="card-body">
                                                                                                                <h5 class="card-title">{{ $discover_item->title }}</h5>
                                                                                                                <p class="card-text">{!! $discover_item->text !!}</p>
                                                                                                                <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $discover_item->created_at->format('d-m-Y') }}</small></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection

@section('scripts')

    <script>
        function showPlanModal(item, discover_id) {
            let items = item.querySelector('.plan-items-body');
            let callback = $('#PlanlaModalEkrani').modal('show');
            let modal_callback = callback[0];
            let content = modal_callback.querySelector('#plan-modal-content');
            content.innerHTML = items.innerHTML;

            let url = ('{{ asset('website_v2/gmap.html') }}');
            let location_uri = ('{{ route('api.discover.location', ':id') }}')
            location_uri = location_uri.replace(':id', discover_id)
            console.log(location_uri)

            $('.mapframe').attr('src', url + '?location_uri='+ location_uri)
        }

        let previous_content = '';

        function showPlanDetail(item) {
            let items = item.querySelector('.plan-item-detail-body');
            previous_content = document.querySelector('#PlanlaModalEkrani #plan-modal-content').innerHTML;
            document.querySelector('#PlanlaModalEkrani #plan-modal-content').innerHTML = items.innerHTML;
            //let callback = $('#PlanlaModalEkrani').modal('show');
            //let modal_callback = callback[0];
            //let content = modal_callback.querySelector('#plan-modal-content');
            //content.innerHTML = items.innerHTML;
        }

        function backPlanModal() {
            document.querySelector('#PlanlaModalEkrani #plan-modal-content').innerHTML = previous_content;
        }
    </script>

@endsection