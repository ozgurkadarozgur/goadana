@extends('layouts.web')

@section('content')

    <div class="tab-pane" id="etkinlik" role="tabpanel" aria-labelledby="etkinlik-tab">

        <div class="hightlighticons button-group filters-button-group" style="background-color:#2c3e50" id="Afiyetle-Filtrele">
            <ul class="list-inline mb-0">
                <!-- For filtering controls add
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                <li class="list-inline-item" data-filter=".kebapciler"><a><i class="fal fa-wine-glass-alt"></i><span>Kebapçılar</span></a> </li>
                <li class="list-inline-item" data-filter=".meyhaneler"><a><i class="fal fa-wine-glass-alt"></i><span>Meyhaneler</span></a> </li>
                <li class="list-inline-item" data-filter=".kahvalti"><a><i class="fal fa-wine-glass-alt"></i><span>Kahvaltı</span></a> </li>
                <li class="list-inline-item" data-filter=".fastfood"><a><i class="fal fa-wine-glass-alt"></i><span>Fast Foods</span></a> </li>
                <li class="list-inline-item" data-filter=".restaurant"><a><i class="fal fa-wine-glass-alt"></i><span>Restaurant</span></a> </li>
                <li class="list-inline-item" data-filter=".yerellezzetler"><a><i class="fal fa-wine-glass-alt"></i><span>Yerel Lezzetler</span></a> </li>
                -->
                <li class="list-inline-item" data-filter="*"><a><i class="fal fa-compress-arrows-alt"></i><span>Hepsi</span></a> </li>
                @foreach($event_types as $event_type)
                    <li class="list-inline-item" data-filter=".{{ Str::slug($event_type->title, '-') }}"><a><i class="{{ $event_type->icon }}"></i><span>{{ $event_type->title }}</span></a> </li>
                @endforeach
            </ul>
        </div>



        <section>
            <div class="tab-5-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#30d5c7;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="min-height:600px">



                            <div class="alert alert-dark" role="alert">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control quicksearch-event" placeholder="Etkinlik ara...">
                                    </div>

                                    <div class="col-lg-3">
                                        <select class="custom-select">
                                            <option selected>Sıralama</option>
                                            <option value="1">Tarihe Göre En Yakın</option>
                                            <option value="2">Tarihe Göre En Uzak</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 text-right">

                                        <a class="btn btn-secondary liste-gorunum-etkinlik" data-toggle="tooltip" data-placement="top" title="Liste Görünümü" style="color:#fff"><i class="fas fa-th-list"></i></a>
                                        <a class="btn btn-secondary harita-gorunum-etkinlik" data-toggle="tooltip" data-placement="top" title="Harita Görünümü" style="color:#fff"><i class="fal fa-map-marked"></i></a>
                                    </div>
                                </div>

                            </div>


                            <div id="liste-gorunum-etkinlik">
                                <div class="grid-etkinlik">
                                    <div class="row filter-container" style="position:relative">

                                        @foreach($events as $event)
                                            <div class="col-lg-3 element-item {{ Str::slug($event->event_type->title, '-') }}" data-category="{{ Str::slug($event->event_type->title, '-') }}">

                                                <div class="card mb-5">
                                                    <img class="img-fluid img-resizing" src="{{ $event->image_url }}" alt="sample" />
                                                    <div class="card-body">
                                                        {{ $event->title }}
                                                        <br>
                                                        <small>{{ \Carbon\Carbon::parse($event->start_date)->isoFormat('LL') }}</small>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div id="harita-gorunum-etkinlik" style="display:none;">
                                <iframe src="" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection

@section('scripts')

    <script>

        $(document).ready(function() {

            let url = ('{{ asset('website_v2/gmap.html') }}');
            let location_uri = ('{{ route('api.event.locations') }}')

            //console.log(location_uri)

            $('iframe').attr('src', url + '?location_uri='+ location_uri)
        })

        slugify = function(text) {
            var trMap = {
                'çÇ':'c',
                'ğĞ':'g',
                'şŞ':'s',
                'üÜ':'u',
                'ıİ':'i',
                'öÖ':'o'
            };
            for(var key in trMap) {
                text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
            }
            return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
                .replace(/\s/gi, "-") // convert spaces to dashes
                .replace(/[-]+/gi, "-") // trim repeated dashes
                .toLowerCase();

        }

        $('.custom-select').change(function () {
            var direction = '';
            if (this.value == 2) direction = 'desc';
            if (this.value == 1) direction = 'asc';
            $.ajax({
                method: 'post',
                url: ('{{ route('api.event.filter-n-sort') }}'),
                data: {
                    sort: {
                        type: 'start_date',
                        direction : direction
                    }
                }
            }).then(res => {
                $('#liste-gorunum-etkinlik .filter-container').html('');
                console.log(res.data)
                let content = '';
                $.each(res.data, function (index, item) {
                    console.log(item)
                    content += '<div class="col-lg-3 element-item '+slugify(item.type)+'" data-category="'+slugify(item.type)+'">' +
                        '                                                <div class="card mb-5">' +
                        '                                                    <img class="img-fluid img-resizing" src="' + item.image_url + '" alt="sample" />' +
                        '                                                    <div class="card-body">' +
                                                                                item.title +
                        '                                                        <br>' +
                        '                                                        <small>'+item.start_date+'</small>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>'
                })
                $('#liste-gorunum-etkinlik .filter-container').html(content);
            })
        })
    </script>

@endsection