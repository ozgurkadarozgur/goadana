<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Rota Adana</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!-- Styles -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('website_v2/css/font-awesome/5.10.2/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/responsive.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <style>
        .tab-1:hover{background-color:#badc58!important;color:#000!important}
        .tab-2:hover{background-color:#95afc0!important;color:#fff!important}
        .tab-2:hover i{color:#fff!important}
        .tab-3:hover{background-color:#2c3e50!important;color:#fff!important}
        .tab-3:hover i{color:#fff!important}
        .tab-4:hover{background-color:#3498db!important;color:#fff!important}
        .tab-4:hover i{color:#fff!important}
        .tab-5:hover{background-color:#30d5c7!important;color:#fff!important}
        .tab-5:hover i{color:#fff!important}
        .tab-6:hover{background-color:#fa9634!important;color:#000!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-1{background-color:#badc58!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-2{background-color:#95afc0!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-3{background-color:#2c3e50!important;color:#fff}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-3 i{color:#fff!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-4{background-color:#3498db!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-5{background-color:#30d5c7!important;color:#fff}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-5 i{color:#fff!important}
        .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active.tab-6{background-color:#fa9634!important}
        .planla h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;top:195px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .kesfet h2{font-size:1rem;color:#000;text-transform:uppercase;letter-spacing:3px;position:absolute;bottom:45px;left:0;margin-left:-30px;-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg);-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;-o-transform-origin:0 0;transform-origin:0 0}
        .filtr-item{margin-bottom:40px!important}
        .blog-wrapper{background:#fff;overflow:hidden;padding:20px;box-shadow:3px 1px 10px rgba(0,0,0,.12)}
        .blog-wrapper p{font-size:14px;line-height:26px}
        .home-blog-wrapper{transition:.3s}
        .blog-wrapper.home-blog-wrapper:hover{box-shadow:3px 1px 10px rgba(0,0,0,.52)}
        .blog-title{display:inline-block;font-size:30px;font-weight:600;line-height:1.4;margin:10px 0 15px;padding:0;text-align:left}
        .blog-content.home-blog h2{font-size:18px;margin-bottom:10px}
        .blog-content.home-blog p{margin-bottom:10px;text-align:justify}
        .link-box.home-blog-link a{font-size:12px;color:#c2cdeb;font-weight:500}
        .blog-title a{color:#555}
        .link-box a{color:#555}
        .link-box a:hover{color:#1c4899}
        .meta-info ul li{color:#9e9e9e;display:inline-block;font-size:11px;padding:0 12px;position:relative;text-transform:uppercase;font-weight:600}
        .embed-responsive{margin-bottom:20px}
        .meta-info ul li:first-child{padding-left:0}
        .meta-info ul li a{color:#ff5e14}
        .meta-info ul li a:hover{color:#444}
        .meta-info ul li::before{border:1px solid #999;border-radius:5px;content:"";height:5px;left:-4px;position:absolute;top:6px;width:5px}
        .meta-info ul li:first-child:before{display:none}
        .blog-thumb img{width:100%}
    </style>
</head>
<body class="full-height" id="scrollup">

<div class="page-loading">
    <img src="{{ asset('website/images/loader.gif') }}" alt="" />
    <span>Yükleniyor...</span>
</div>

<div class="theme-layout">

    <section>
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ asset('website/images/resource/p4.jpg') }}) repeat scroll 50% 422.28px transparent;" class="no-parallax parallax scrolly-invisible"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-lg-10">
                        <div class="listingsf lowmargin">
                            <h2 class="text-center mx-auto center-block"><img src="{{ asset('website/images/inadana-logo.png') }}" style="float:none!important" class="img-fluid"></h2>
                            <h3 style="text-shadow: 1px 1px 2px rgba(0,0,0,0.8)">Şehirlerin <span style="font-size:50px">01</span> Numarasını Keşfedin...</h3>
                            <p style="text-shadow: 1px 1px 2px rgba(0,0,0,0.8)">Şehrin uzmanlarından; konaklamak, yemek yemek, alışveriş yapmak veya ziyaret etmek için harika yerler bulun.</p>

                            <div class="browsehighlights">
                                <span>veya Kategorileri İncele</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white mt-5" id="sekmeler">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link tab-1" id="planla-tab" data-toggle="tab" href="#planla" role="tab" aria-controls="planla" aria-selected="true"><span style="color:#c3dede;font-size:30px;margin-right:10px">01</span> <i class="fal fa-edit" style="color:#000;font-size:30px"></i> Planla</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-2" id="kesfet-tab" data-toggle="tab" href="#kesfet" role="tab" aria-controls="kesfet" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">02</span> <i class="fab fa-sistrix" style="color:#000;font-size:30px"></i> Keşfet</a>
                                </li>
                                <li class="nav-item afiyetle">
                                    <a class="nav-link tab-3" id="afiyetle-tab" data-toggle="tab" href="#afiyetle" role="tab" aria-controls="afiyetle" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">03</span> <i class="fal fa-utensils-alt" style="color:#000;font-size:30px"></i> Afiyetle</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-4" id="anla-tab" data-toggle="tab" href="#anla" role="tab" aria-controls="anla" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">04</span> <i class="fal fa-map-signs" style="color:#000;font-size:30px"></i> Anla</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-5" id="etkinlik-tab" data-toggle="tab" href="#etkinlik" role="tab" aria-controls="etkinlik" aria-selected="false"><span style="color:#c3dede;font-size:30px;margin-right:10px">05</span> <i class="fal fa-calendar-alt" style="color:#000;font-size:30px"></i> Etkinlikler</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab-6" id="harita-tab" data-toggle="tab" href="#harita" role="tab" aria-controls="harita" aria-selected="false" style="border:none"><span style="color:#c3dede;font-size:30px;margin-right:10px">06</span> <i class="fal fa-map-marked-alt" style="color:#000;font-size:30px"></i> Haritada Gör</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="anasayfa" role="tabpanel" aria-labelledby="anasayfa-tab">


            <section>
                <!--<div class="tab-1-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#badc58;right:40px;top:60px"></div>-->
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hpsec">
                                    <div class="planla">
                                        <h2>PLANLA</h2>
                                        <div class="row">
                                            @foreach($plans as $plan)
                                                @if($loop->iteration == 1)
                                                    <div class="col-lg-6">
                                                        <div class="cities still">
                                                            <a href="#" title=""><img class="img-resizing" src="{{ $plan->image_url }}" alt="" /></a>
                                                            <div class="cities-title"><h3><a href="#" title="">{{ $plan->title }}</a></h3></div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-lg-3">
                                                    <div class="cities still">
                                                        <a href="#" title=""><img src="{{ $plan->image_url }}" alt="" /></a>
                                                        <div class="cities-title"><h3><a href="#" title="">{{ $plan->title }}</a></h3></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            {{--<div class="col-lg-3">
                                                <div class="cities still">
                                                    <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb1.jpg') }}" alt="" /></a>
                                                    <div class="cities-title"><h3><a href="#" title="">Tufanbeyli</a></h3></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="cities still">
                                                    <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb2.jpg') }}" alt="" /></a>
                                                    <div class="cities-title"><h3><a href="#" title="">Çukurova</a></h3></div>
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                    <div class="kesfet">
                                        <h2>KEŞFET</h2>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="cities still">
                                                    <a href="#" title=""><img src="{{ asset('website/images/resource/hps2.jpg') }}" alt="" /></a>
                                                    <div class="cities-title"><h3><a href="#" title="">Kozan</a></h3></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="cities still">
                                                    <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb3.jpg') }}" alt="" /></a>
                                                    <div class="cities-title"><h3><a href="#" title="">Kadirli</a></h3></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="cities still">
                                                    <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb4.jpg') }}" alt="" /></a>
                                                    <div class="cities-title"><h3><a href="#" title="">Pozantı</a></h3></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>

        <div class="tab-pane fade" id="planla" role="tabpanel" aria-labelledby="planla-tab">


            <section>
                <div class="tab-1-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#badc58;right:40px;top:60px"></div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="hpsec">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hps1.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Hong Kong</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb1.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Pakistan</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb2.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Malaysia</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb3.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Netherland</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb4.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Turkey</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hps2.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Dubai</a></h3></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
        <div class="tab-pane fade" id="kesfet" role="tabpanel" aria-labelledby="kesfet-tab">

            <section>
                <div class="tab-2-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#95afc0;right:40px;top:60px"></div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="hpsec">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hps1.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Hong Kong</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb1.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Pakistan</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb2.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Malaysia</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb3.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Netherland</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="cities still">
                                                <a href="#" title=""><img src="{{ asset('website/images/resource/hpsb4.jpg') }}" alt="" /></a>
                                                <div class="cities-title"><h3><a href="#" title="">Turkey</a></h3></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="margin-top:30px;padding-top:10px;padding-bottom:10px;border: 2px dashed #ddd;">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <h6 class="mb-1">Açıklama</h6>
                                                    <div class="ldesc">
                                                        <p>Seyyahların gözüyle Adana<br>
                                                            Ticaret yollarının üzerinde yer almasından dolayı Adanada bir çok gezginin uğradığı bir yer olmuştur.
                                                            El-Gazzi Arap seyyah 1500'lü yıllarda Adana'ya yaptığı ziyaretlerde, Seyhan nehrinin akışını, çöreklenmiş bir yılana benzetir. <span class="badge badge-dark" data-toggle="modal" data-target="#ModalEkrani">Devamı...</span></p>
                                                    </div>
                                                    <div class="amenties mt-3">
                                                        <span><i class="flaticon-credit-card"></i>Müze Kartı Geçerli</span>
                                                        <span><i class="flaticon-wifi"></i>Rezervasyon Gerektirmez</span>
                                                        <span><i class="flaticon-parking"></i>Açık Alan</span>
                                                        <span><i class="flaticon-bicycle"></i>Bisiklet Yolu</span>
                                                        <span><i class="flaticon-handicapped"></i>Engelli Rampası</span>
                                                        <span><i class="flaticon-booked"></i>Rezervasyon</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <h6 class="mb-1">Haritada</h6>
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d199978.90557016447!2d27.0419847!3d38.4490815!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b963c07a179e19%3A0x484b4db6e8d1cf26!2zRWRlbCBIdWt1ayAmIERhbsSxxZ9tYW5sxLFr!5e0!3m2!1str!2str!4v1579990154887!5m2!1str!2str" width="100%" height="120" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                                    <span style="font-size: 12px;color: #8d8d8d;">Kıralan, 01170 Karaisalı/Adana</span>
                                                    <div class="wsocial">
                                                        <a href="#" title=""><i class="fa fa-facebook"></i></a>
                                                        <a href="#" title=""><i class="fa fa-twitter"></i></a>
                                                        <a href="#" title=""><i class="fa fa-linkedin"></i></a>
                                                        <a href="#" title=""><i class="fa fa-pinterest"></i></a>
                                                        <a href="#" title=""><i class="fa fa-google"></i></a>
                                                        <a href="#" title=""><i class="fa fa-dribbble"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
        <div class="tab-pane fade" id="afiyetle" role="tabpanel"  aria-labelledby="afiyetle-tab">



            <div class="hightlighticons button-group filters-button-group" style="background-color:#2c3e50" id="Afiyetle-Filtrele">
                <ul class="list-inline mb-0">
                    <!-- For filtering controls add -->
                    <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>

                   {{-- <li class="list-inline-item" data-filter=".kebap"><a><i class="fal fa-wine-glass-alt"></i><span>Kebap</span></a> </li>
                    <li class="list-inline-item" data-filter=".fastfood"><a><i class="fal fa-wine-glass-alt"></i><span>Fast Food</span></a> </li>
                    <li class="list-inline-item" data-filter=".deneme"><a><i class="fal fa-wine-glass-alt"></i><span>Deneme</span></a> </li>
                   --}}
                    @foreach($place_types as $place_type)
                        <li class="list-inline-item" data-filter=".{{ Str::slug($place_type->title, '-') }}"><a><i class="fal fa-wine-glass-alt"></i><span>{{ $place_type->title }}</span></a> </li>
                    @endforeach
                </ul>
            </div>


            <section>
                <div class="tab-3-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#2c3e50;right:40px;top:60px"></div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="alert alert-dark" role="alert">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control quicksearch" placeholder="İsim veya ilçe ile arayın...">
                                        </div>

                                        <div class="col-lg-3 offset-lg-3 text-right">

                                            <a class="btn btn-secondary liste-gorunum" data-toggle="tooltip" data-placement="top" title="Liste Görünümü" style="color:#fff"><i class="fas fa-th-list"></i></a>
                                            <a class="btn btn-secondary harita-gorunum" data-toggle="tooltip" data-placement="top" title="Harita Görünümü" style="color:#fff"><i class="fal fa-map-marked"></i></a>
                                        </div>
                                    </div>

                                </div>

                                <div id="liste-gorunum">
                                    <div class="grid">
                                        <div class="row filter-container" style="position:relative">

                                            @foreach($places as $place)
                                                @php
                                                $category = Str::slug($place->place_type_matches->first()->place_type->title, '-')
                                                @endphp
                                                <div class="col-lg-3 element-item {{ $place->slug_type_matches() }}" data-category="{{ $place->slug_type_matches() }}">
                                                    <div class="card mb-5">
                                                        <img src="{{ $place->image_url }}" alt="sample" />
                                                        <div class="card-body">
                                                            {{ $place->title }}
                                                            <br>
                                                            <small>Çukurova</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                         {{--   <div class="col-lg-3 element-item  fastfood" data-category="fastfood">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Mama Craft
                                                        <br>
                                                        <small>Çukurova</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item  fastfood" data-category="fastfood">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        MC Donalds
                                                        <br>
                                                        <small>Seyhan</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 element-item  kebap" data-category="kebap">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Atlas Kebap
                                                        <br>
                                                        <small>Seyhan</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item fastfood" data-category="fastfood">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Burger King
                                                        <br>
                                                        <small>Çukurova,Seyhan</small>
                                                    </div>
                                                </div>
                                            </div>--}}

                                        </div>
                                    </div>
                                </div>


                                <div id="harita-gorunum" style="display:none;">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3186.7631800898125!2d35.33203111503039!3d36.9915856646031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15288f6fae5158d7%3A0x6aeaffe69e65a068!2sSabanc%C4%B1%20Merkez%20Camii!5e0!3m2!1str!2str!4v1584099409284!5m2!1str!2str" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>


        <div class="tab-pane fade" id="anla" role="tabpanel" aria-labelledby="anla-tab">

            <section>
                <div class="tab-4-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#3498db;right:40px;top:60px"></div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12" style="min-height:600px">

                                <!-- Anla Modal -->
                                <div class="modal fade" id="anlaBlogDetay" tabindex="-1" role="dialog" aria-labelledby="anlaModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="anlaModalLabel">Blog Başlık</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lectus nulla, venenatis nec dui in, scelerisque suscipit mauris. Cras at euismod quam, eu accumsan nunc.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light" data-dismiss="modal">Kapat</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    @foreach($get_its as $get_it)
                                        <div class="col-xl-4 col-lg-4 col-md-6">
                                            <div class="blog-wrapper home-blog-wrapper white-bg">
                                                <div class="meta-info">
                                                    <ul class="mb-0">
                                                        <li class="posts-time">{{ $get_it->created_at }}</li>
                                                    </ul>
                                                </div>
                                                <div class="blog-content home-blog">
                                                    <img src="{{ $get_it->image_url }}" class="img-fluid">
                                                    <h2 class="blog-title">{{ $get_it->title }}</h2>
                                                    <p>{!! $get_it->text !!}</p>
                                                </div>
                                                <div class="link-box home-blog-link">
                                                    <a href="#" data-toggle="modal" data-target="#anlaBlogDetay">Ayrıntılar</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>

        <div class="tab-pane fade" id="etkinlik" role="tabpanel" aria-labelledby="etkinlik-tab">

            <div class="hightlighticons button-group filters-button-group" style="background-color:#2c3e50" id="Afiyetle-Filtrele">
                <ul class="list-inline mb-0">
                    <!-- For filtering controls add -->
                    <li class="list-inline-item" data-filter="*"><a><i class="fal fa-wine-glass-alt"></i><span>Hepsi</span></a> </li>
                    <li class="list-inline-item" data-filter=".kebapciler"><a><i class="fal fa-wine-glass-alt"></i><span>Kebapçılar</span></a> </li>
                    <li class="list-inline-item" data-filter=".meyhaneler"><a><i class="fal fa-wine-glass-alt"></i><span>Meyhaneler</span></a> </li>
                    <li class="list-inline-item" data-filter=".kahvalti"><a><i class="fal fa-wine-glass-alt"></i><span>Kahvaltı</span></a> </li>
                    <li class="list-inline-item" data-filter=".fastfood"><a><i class="fal fa-wine-glass-alt"></i><span>Fast Foods</span></a> </li>
                    <li class="list-inline-item" data-filter=".restaurant"><a><i class="fal fa-wine-glass-alt"></i><span>Restaurant</span></a> </li>
                    <li class="list-inline-item" data-filter=".yerellezzetler"><a><i class="fal fa-wine-glass-alt"></i><span>Yerel Lezzetler</span></a> </li>
                </ul>
            </div>



            <section>
                <div class="tab-5-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#30d5c7;right:40px;top:60px"></div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12" style="min-height:600px">



                                <div class="alert alert-dark" role="alert">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control quicksearch" placeholder="Etkinlik adı veya ücret durumunu ile ara...">
                                        </div>

                                        <div class="col-lg-3">
                                            <select class="custom-select">
                                                <option selected>Sıralama</option>
                                                <option value="1">Tarihe Göre En Yakın</option>
                                                <option value="1">Tarihe Göre En Uzak</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 text-right">

                                            <a class="btn btn-secondary liste-gorunum" data-toggle="tooltip" data-placement="top" title="Liste Görünümü" style="color:#fff"><i class="fas fa-th-list"></i></a>
                                            <a class="btn btn-danger harita-gorunum" data-toggle="tooltip" data-placement="top" title="Harita Görünümü" style="color:#fff"><i class="fal fa-map-marked"></i></a>
                                        </div>
                                    </div>

                                </div>


                                <div id="liste-gorunum">
                                    <div class="grid-etkinlik">
                                        <div class="row filter-container" style="position:relative">

                                            <div class="col-lg-3 element-item  kebap" data-category="kebap">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 1
                                                        <br>
                                                        <small>ücretli</small>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-lg-3 element-item kebapcilar" data-category="kebapcilar">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 2
                                                        <br>
                                                        <small>ücretli</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item  meyhaneler" data-category="meyhaneler">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 3
                                                        <br>
                                                        <small>ücretli</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 element-item  kahvalti" data-category="kahvalti">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 4
                                                        <br>
                                                        <small>ücretsiz</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item kahvalti" data-category="kahvalti">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 5
                                                        <br>
                                                        <small>ücretsiz</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item kahvalti" data-category="kahvalti">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 6
                                                        <br>
                                                        <small>ücretsiz</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item restaurant" data-category="restaurant">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 7
                                                        <br>
                                                        <small>ücretli</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item restaurant" data-category="restaurant">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 8
                                                        <br>
                                                        <small>ücretli</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 element-item restaurant" data-category="restaurant">
                                                <div class="card mb-5">
                                                    <img src="http://placehold.it/450x300" alt="sample" />
                                                    <div class="card-body">
                                                        Etkinlik 9
                                                        <br>
                                                        <small>ücretli</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>







                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>

        <div class="tab-pane fade" id="harita" role="tabpanel" aria-labelledby="harita-tab">
            <section>
                <div class="tab-6-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#fa9634;right:40px;top:60px"></div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12" style="min-height:600px">
                                <div id="map_div" class="map">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>

    <footer class="dfooter">
        <div class="block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 column">
                        <div class="fabout">
                            <a href="#" title=""><img src="{{ asset('website/images/inadana-logo.png') }}" alt="" /></a>
                            <span>Seyhan, Adana</span>
                            <span>+90 322 111 22 33</span>
                        </div>
                    </div>
                    <div class="col-lg-2 offset-lg-1 column">
                        <h3 class="ftitle">Ne Yapıyoruz?</h3>
                        <div class="flink">
                            <a href="#" title="">Planla</a>
                            <a href="#" title="">Keşfet</a>
                            <a href="#" title="">Anla</a>
                            <a href="#" title="">Haritada Gör</a>
                            <a href="#" title="">İletişim</a>
                        </div>
                    </div>
                    <div class="col-lg-3 column">
                        <h3 class="ftitle">Mekanlar</h3>
                        <div class="flink">
                            <a href="#" title="">Kahvaltı</a>
                            <a href="#" title="">Restorant</a>
                            <a href="#" title="">Fast Foods</a>
                            <a href="#" title="">Kebapçılar</a>
                            <a href="#" title="">Yöresel Lezzet</a>
                        </div>
                    </div>
                    <div class="col-lg-3 column">
                        <h3 class="ftitle">Uygulamamızı İndirin</h3>
                        <div class="dbuttons">
                            <a href="#" title=""><img src="{{ asset('website/images/ios.png') }}" alt="" /></a>
                            <a href="#" title=""><img src="{{ asset('website/images/google.png') }}" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottomline s2">
            <a href="#scrollup" class="scrollup s2" title=""><i class="fa fa-angle-up"></i></a>
            <div class="container">
                <span>© 2020 InAdana | Her hakkı saklıdır.</span>
                <div class="social">
                    <a href="#" title=""><i class="fab fa-facebook"></i></a>
                    <a href="#" title=""><i class="fab fa-twitter"></i></a>
                    <a href="#" title=""><i class="fab fa-linkedin"></i></a>
                    <a href="#" title=""><i class="fab fa-pinterest"></i></a>
                    <a href="#" title=""><i class="fab fa-google"></i></a>
                    <a href="#" title=""><i class="fab fa-github"></i></a>
                </div>
            </div>
        </div>
    </footer>

</div>



<div class="clearfix"></div>
<!-- Modal -->
<div class="modal fade" id="ModalEkrani" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body" style="padding:0">
                <nav style="float:none">
                    <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" style="border-bottom:1px solid #dee2e6;border-right:1px solid #dee2e6" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">İncele</a>
                        <a class="nav-item nav-link"  style="border-bottom:1px solid #dee2e6;border-right:none" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Haritada Gör</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div style="padding:20px;margin-bottom:30px">
                            <h6 class="mb-1">Açıklama <i class="fab fa-whatsapp"></i></h6>
                            <div class="ldesc">
                                <p>Seyyahların gözüyle Adana<br>
                                    Ticaret yollarının üzerinde yer almasından dolayı Adanada bir çok gezginin uğradığı bir yer olmuştur.
                                    El-Gazzi Arap seyyah 1500'lü yıllarda Adana'ya yaptığı ziyaretlerde, Seyhan nehrinin akışını, çöreklenmiş bir yılana benzetir. <span class="badge badge-dark">Devamı...</span></p>
                            </div>
                            <div class="amenties mt-3 mb-4">
                                <span><i class="flaticon-credit-card"></i>Müze Kartı Geçerli</span>
                                <span><i class="flaticon-wifi"></i>Rezervasyon Gerektirmez</span>
                                <span><i class="flaticon-parking"></i>Açık Alan</span>
                                <span><i class="flaticon-bicycle"></i>Bisiklet Yolu</span>
                                <span><i class="flaticon-handicapped"></i>Engelli Rampası</span>
                                <span><i class="flaticon-booked"></i>Rezervasyon</span>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3186.7631800898125!2d35.33203111503039!3d36.9915856646031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15288f6fae5158d7%3A0x6aeaffe69e65a068!2sSabanc%C4%B1%20Merkez%20Camii!5e0!3m2!1str!2str!4v1584099409284!5m2!1str!2str" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('website/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.sabahservers.com/eklenti/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('website/js/isotope-docs.min.js?6') }}"></script>
<script src="{{ asset('website/js/script.js') }}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxjqcD2kYEBrF9g24eZzzyofKAl3tdEPo"type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('website/js/map1.js') }}"></script>


<script>

    $(document).ready(function(){



// init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });

// init Isotope
        var $gridetkinlik = $('.grid-etkinlik').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });
// filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function() {
                var number = $(this).find('.number').text();
                return parseInt( number, 10 ) > 50;
            },
            // show if name ends with -ium
            ium: function() {
                var name = $(this).find('.name').text();
                return name.match( /ium$/ );
            }
        };


// bind filter button click
        $('.filters-button-group').on( 'click', 'li', function() {
            var filterValue = $( this ).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[ filterValue ] || filterValue;
            $grid.isotope({ filter: filterValue });
            $gridetkinlik.isotope({ filter: filterValue });
        });
// change is-checked class on buttons
        $('.button-group').each( function( i, buttonGroup ) {
            var $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'li', function() {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $( this ).addClass('is-checked');
            });
        });


// quick search regex
        var qsRegex;

// use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            var $grid = $('.grid').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows',
                filter: function() {
                    return qsRegex ? $(this).text().match( qsRegex ) : true;
                }
            });
            var $gridetkinlik = $('.grid-etkinlik').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows',
                filter: function() {
                    return qsRegex ? $(this).text().match( qsRegex ) : true;
                }
            });
            $grid.isotope();
            $gridetkinlik.isotope();

        }, 200 ) );

// debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            threshold = threshold || 100;
            return function debounced() {
                clearTimeout( timeout );
                var args = arguments;
                var _this = this;
                function delayed() {
                    fn.apply( _this, args );
                }
                timeout = setTimeout( delayed, threshold );
            };
        }

        $('.nav-tabs a').on('shown.bs.tab', function(){
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#sekmeler").offset().top
            }, 200);
            $grid.isotope({ filter: '*' });
            $gridetkinlik.isotope({ filter: '*' });
        });








        $(".harita-gorunum").click(function() {
            $("#liste-gorunum").hide();
            $("#harita-gorunum").show();
        });

        $(".liste-gorunum").click(function() {
            $("#liste-gorunum").show();
            $("#harita-gorunum").hide();
        });



    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
</body>
</html>
