@extends('layouts.web')

@section('content')

    <div class="tab-pane" id="anla" role="tabpanel" aria-labelledby="anla-tab">

        <section>
            <div class="tab-4-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#3498db;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="min-height:600px">

                            <!-- Anla Modal -->
                            <div class="modal fade" id="anlaBlogDetay" tabindex="-1" role="dialog" aria-labelledby="anlaModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lectus nulla, venenatis nec dui in, scelerisque suscipit mauris. Cras at euismod quam, eu accumsan nunc.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light" data-dismiss="modal">Kapat</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                @foreach($get_its as $get_it)
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="blog-wrapper home-blog-wrapper white-bg">
                                            <div class="meta-info">
                                                <ul class="mb-0">
                                                    <li class="posts-time">{{ $get_it->created_at->isoFormat('LL') }}</li>
                                                </ul>
                                            </div>
                                            <div class="blog-content home-blog" style="cursor: pointer;" onclick="showAnlaModal(this.parentNode)">
                                                <img src="{{ $get_it->image_url }}" class="img-fluid img-resizing">
                                                <h2 class="blog-title">{{ $get_it->title }}</h2>
                                            </div>
                                            <div class="link-box home-blog-link">
                                                <a href="#" onclick="showAnlaModal(this.parentNode)">Ayrıntılar</a>
                                                <div class="anla-content" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card mb-3">
                                                                <img class="card-img-top img-fluid img-resizing" src="{{ $get_it->image_url }}" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h5 class="card-title">{{ $get_it->title }}</h5>
                                                                    <p class="card-text">{!! $get_it->text !!}</p>
                                                                    <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $get_it->created_at->format('d-m-Y') }}</small></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection

@section('scripts')

    <script>
        function showAnlaModal(item) {
            //console.log(item)

            let items = item.querySelector('.anla-content');
            let callback = $('#anlaBlogDetay').modal('show');
            let modal_callback = callback[0];
            let content = modal_callback.querySelector('#anlaBlogDetay .modal-body');
            content.innerHTML = items.innerHTML;

        }

    </script>

@endsection