@extends('layouts.web')

@section('content')

    <div class="tab-pane" id="harita" role="tabpanel" aria-labelledby="harita-tab">
        <section>
            <div class="tab-6-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#fa9634;right:40px;top:60px"></div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="min-height:600px">
                            <iframe src="" width="100%" scrolling="no" height="600" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('scripts')

    <script>

        $(document).ready(function() {

            let url = ('{{ asset('website_v2/gmap.html') }}');
            let location_uri = ('{{ route('api.place_type.locations') }}')

            $('iframe').attr('src', url + '?location_uri='+ location_uri)
        })

    </script>

@endsection