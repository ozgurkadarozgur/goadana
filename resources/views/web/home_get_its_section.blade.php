<div class="row">

    <!-- Anla Modal -->
    <div class="modal fade" id="anlaBlogDetay" tabindex="-1" role="dialog" aria-labelledby="anlaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lectus nulla, venenatis nec dui in, scelerisque suscipit mauris. Cras at euismod quam, eu accumsan nunc.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>


@foreach($get_its as $get_it)
        <div class="{{ $loop->iteration == 1 ? 'col-lg-6' : 'col-lg-3' }}">
            <div class="cities still">
                <a href="#" onclick="showAnlaModal(this.parentNode)" title="asd"><img class="img-fluid img-resizing" src="{{ $get_it->image_url }}" alt="" /></a>
                <div onclick="showAnlaModal(this.parentNode)" class="cities-title"><h3><a href="#" title="">{{ $get_it->title }}</a></h3></div>
                <div class="anla-content" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card mb-3">
                                <img class="card-img-top img-fluid img-resizing" src="{{ $get_it->image_url }}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $get_it->title }}</h5>
                                    <p class="card-text">{!! $get_it->text !!}</p>
                                    <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $get_it->created_at->format('d-m-Y') }}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>