@extends('layouts.web')

@section('content')

    <div class="tab-pane fade show active" id="anasayfa" role="tabpanel" aria-labelledby="anasayfa-tab">


        <section>
            <!--<div class="tab-1-bar" style="position:absolute;width:10px;height:450px;display:block;background-color:#badc58;right:40px;top:60px"></div>-->
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpsec">
                                <div class="planla">
                                    <h2>PLANLA</h2>
                                    @include('web.home_plan_section')
                                </div>
                                <div class="kesfet">
                                    <h2>KEŞFET</h2>
                                    @include('web.home_discover_section')
                                </div>
                                <div class="afiyetle">
                                    <h2>AFİYETLE</h2>
                                    @include('web.home_places_section')
                                </div>
                                <div class="anla">
                                    <h2>ANLA</h2>
                                    @include('web.home_get_its_section')
                                </div>
                                <div class="etkinlik">
                                    <h2>ETKİNLİKLER</h2>
                                    @include('web.home_events_section')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection

@section('scripts')

    <script>
        function showPlanModal(item, plan_id) {

            let items = item.querySelector('.plan-items-body');
            let callback = $('#PlanlaModalEkrani').modal('show');
            let modal_callback = callback[0];
            let content = modal_callback.querySelector('#plan-modal-content');
            content.innerHTML = items.innerHTML;

            let url = ('{{ asset('website_v2/gmap.html') }}');
            let location_uri = ('{{ route('api.plan.location', ':id') }}')
            location_uri = location_uri.replace(':id', plan_id)
            console.log(location_uri)

            $('iframe').attr('src', url + '?location_uri='+ location_uri)

        }

        let previous_content = '';

        function showPlanDetail(item) {
            let items = item.querySelector('.plan-item-detail-body');
            previous_content = document.querySelector('#PlanlaModalEkrani #plan-modal-content').innerHTML;
            document.querySelector('#PlanlaModalEkrani #plan-modal-content').innerHTML = items.innerHTML;
            //let callback = $('#PlanlaModalEkrani').modal('show');
            //let modal_callback = callback[0];
            //let content = modal_callback.querySelector('#plan-modal-content');
            //content.innerHTML = items.innerHTML;
        }

        function backPlanModal() {
            if (previous_content != '') document.querySelector('#PlanlaModalEkrani #plan-modal-content').innerHTML = previous_content;
        }

        function showAnlaModal(item) {
            console.log(item)

            let items = item.querySelector('.anla-content');
            let callback = $('#anlaBlogDetay').modal('show');
            let modal_callback = callback[0];
            let content = modal_callback.querySelector('#anlaBlogDetay .modal-body');
            content.innerHTML = items.innerHTML;

        }

    </script>

@endsection