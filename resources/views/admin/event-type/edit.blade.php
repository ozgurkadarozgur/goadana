@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('event-type.edit', $event_type) }}
        <h4>{{ $event_type->title }}</h4>
        <hr />

        <form method="post" action="{{ route('admin.event-type.update', $event_type->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Etkinlik Türü</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Adı" value="{{ $event_type->title }}">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection