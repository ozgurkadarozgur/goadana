@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('event-type.create') }}

        <h3>Etkinlik Türü Ekle</h3>

        <hr />

        <form method="post" action="{{ route('admin.event-type.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Etkinlik Türü</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Etkinlik Türü">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>

            <div class="form-group">
                <label for="title">Resim</label>
                <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">
                <div class="invalid-feedback">
                    {{ $errors->first('image') }}
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Ekle</button>
        </form>
    </div>

@endsection