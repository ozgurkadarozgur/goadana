@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('event-type.show', $event_type) }}
        @include('admin.event-type.modal.confirm_event_type_destroy_modal')
        @include('admin.event-type.modal.create_event_modal')
        @include('admin.event-type.modal.update_event_type_photo_modal')

        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $event_type->title }}</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.event-type.edit', $event_type->id) }}">Düzenle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-event-type-photo-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Değiştir</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#destroy-event-type-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#" data-toggle="modal" data-target="#create-event-modal" data-backdrop="static" data-keyboard="false">Yeni Etkinlik Ekle</a>--}}
                        {{--<a class="dropdown-item" href="#" data-toggle="modal" data-target="#add-from-existing-places-modal" data-backdrop="static" data-keyboard="false">Mevcut Mekanlardan Ekle</a>--}}
                    </div>
                </div>

            </div>
        </div>

        <img class="card-img-top" src="{{ $event_type->image_url }}" alt="Card image cap">

        <hr id="line" />



        <div class="row">
            <div class="col-md-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">&#x1f50d;</span>
                    </div>
                    <input id="search-text" onfocus="scrollToElement(this)" onkeyup="search(this.value.toLowerCase())" type="text" class="form-control" placeholder="Etkinlik Ara..." aria-label="search" aria-describedby="basic-addon1">
                </div>
            </div>
            @foreach($event_type->events as $item)
                <div class="col-md-4 mb-3 card-item-container">
                    <div class="card event">
                        <img src="{{ $item->image_url }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{ route('admin.event.show', $item->id) }}" class="event-title">
                                    {{ $item->title }}
                                </a>
                            </h5>
                            <p class="card-text">{{ 'Etkinlik Tarihi: ' . \Carbon\Carbon::parse($item->start_date)->isoFormat('LL') }}</p>
                            <p class="card-text">{!! $item->description !!}</p>
                            <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $item->created_at->format('d-m-Y') }}</small></p>
                            {{--<span class="badge badge-danger" style="cursor: pointer;" onclick="setEvent('{{ $item->id }}', '{{ $item->title }}')" data-toggle="modal" data-target="#remove-place-from-place-type-modal" data-backdrop="static" data-keyboard="false">Kaldır</span>--}}
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>

@endsection

@section('scripts')

    <script>
        function setEvent(id, title) {
            document.getElementById('place-id').value = id;
            document.getElementById('place-text').innerText = title;
        }

        function scrollToElement() {
            let el = document.getElementById('line');
            el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
        }

        function search(keyword) {
            scrollToElement();
            let events = document.getElementsByClassName('event');
            let event_arr = Array.from(events);
            event_arr.filter(function (item) {
                let event_title = item.getElementsByClassName('event-title')[0].text.toLowerCase();
                if (event_title.indexOf(keyword) < 0) item.parentElement.style.display = 'none';
                else item.parentElement.style.display = '';
            })
        }

    </script>

@endsection