@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('get-it.show', $get_it) }}
        @include('admin.get-it.modal.confirm_get_it_destroy_modal')
        @include('admin.get-it.modal.update_get_it_photo_modal')
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $get_it->title }}</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.get-it.edit', $get_it->id) }}">Düzenle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-get-it-photo-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Değiştir</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#destroy-get-it-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                    </div>
                </div>

            </div>
        </div>

        <hr />

        <img class="card-img-top" src="{{ $get_it->image_url }}" alt="Card image cap">

        <p>{!! $get_it->text !!}</p>

    </div>

@endsection