@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('get-it.edit', $get_it) }}
        <h4>{{ $get_it->title }}</h4>
        <hr />

        <form method="post" action="{{ route('admin.get-it.update', $get_it->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Başlık</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık" value="{{ $get_it->title }}">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>
            <div class="form-group">
                <label for="description">İçerik</label>
                <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik">{{ $get_it->text }}</textarea>
                <div class="invalid-feedback">
                    {{ $errors->first('text') }}
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('text');
            CKEDITOR.editorConfig = function( config ) {
                config.toolbarGroups = [
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'others', groups: [ 'others' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'about', groups: [ 'about' ] }
                ];

                config.removeButtons = 'Underline,Subscript,Superscript,Image';
            };
        }, 100);

    </script>

@endsection