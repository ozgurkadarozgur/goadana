@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('get-it.create') }}

        <h3>Yeni Ekle</h3>

        <hr />

        <form method="post" action="{{ route('admin.get-it.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="image">Resim</label>
                <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">
                <div class="invalid-feedback">
                    {{ $errors->first('image') }}
                </div>
            </div>

            <div class="form-group">
                <label for="title">Başlık</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>
            <div class="form-group">
                <label for="text">İçerik</label>
                <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik"></textarea>
                <div class="invalid-feedback">
                    {{ $errors->first('text') }}
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Ekle</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('text');
        }, 100);

    </script>

@endsection