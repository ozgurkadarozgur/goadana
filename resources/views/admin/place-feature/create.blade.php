@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place-feature.create') }}

        <h3>Mekan Özelliği Ekle</h3>

        <hr />

        <form method="post" action="{{ route('admin.place-feature.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="title">İkon</label>
                <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image" >
                <div class="invalid-feedback">
                    {{ $errors->first('image') }}
                </div>
            </div>

            <div class="form-group">
                <label for="title">Mekan Özelliği</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Özelliği">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Ekle</button>
        </form>
    </div>

@endsection