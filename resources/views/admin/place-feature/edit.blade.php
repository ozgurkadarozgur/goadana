@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place-type.edit', $place_type) }}
        <h4>{{ $place_type->title }}</h4>
        <hr />

        <form method="post" action="{{ route('admin.place-type.update', $place_type->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Mekan Türü</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Adı" value="{{ $place_type->title }}">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection