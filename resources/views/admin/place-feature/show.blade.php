@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place-type.show', $place_type) }}
        @include('admin.place-type.modal.confirm_place_type_destroy_modal')
        @include('admin.place-type.modal.create_place_modal')
        @include('admin.place-type.modal.add_from_existing_places_modal')
        @include('admin.place-type.modal.confirm_remove_place_from_place_type_modal')
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $place_type->title }}</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.place-type.edit', $place_type->id) }}">Düzenle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#destroy-place-type-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#create-place-modal" data-backdrop="static" data-keyboard="false">Yeni Mekan Ekle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#add-from-existing-places-modal" data-backdrop="static" data-keyboard="false">Mevcut Mekanlardan Ekle</a>
                    </div>
                </div>

            </div>
        </div>

        <hr />

        <div class="row">

            @foreach($place_type->place_matches as $item)
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <img src="{{ asset('img/entertainment.jpg') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ route('admin.place.show', $item->place->id) }}">{{ $item->place->title }}</a></h5>
                            @foreach($item->place->place_type_matches as $match_item)
                                <span class="badge badge-success">{{ $match_item->place_type->title }}</span>
                            @endforeach
                            <p class="card-text">{{ $item->place->description }}</p>
                            <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $item->place->created_at->format('d-m-Y') }}</small></p>
                            <span class="badge badge-danger" style="cursor: pointer;" onclick="setPlace('{{ $item->place->id }}', '{{ $item->place->title }}')" data-toggle="modal" data-target="#remove-place-from-place-type-modal" data-backdrop="static" data-keyboard="false">Bu kategoriden kaldır.</span>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>

@endsection

@section('scripts')

    <script>
        function setPlace(id, title) {
            document.getElementById('place-id').value = id;
            document.getElementById('place-text').innerText = title;
        }
    </script>

@endsection