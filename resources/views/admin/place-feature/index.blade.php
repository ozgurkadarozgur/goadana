@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place-feature.index') }}

        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">Mekan Özellikleri</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.place-feature.create') }}">Yeni Ekle</a>
                    </div>
                </div>
            </div>
        </div>

        <hr />

        <ul class="list-group">
            @foreach($place_features as $item)
                <li class="list-group-item">
                    <b>{{ $loop->iteration . ')' }}</b>
                    {{ $item->title }}
                    <img src="{{ $item->icon_url }}" alt="..." width="32" height="32">
                    <div class="float-right">
                        <a href="{{ route('admin.place-feature.show', $item->id) }}">İncele</a>
                        |
                        <a href="{{ route('admin.place-feature.edit', $item->id) }}">Düzenle</a>
                    </div>
                </li>
            @endforeach
        </ul>

    </div>

@endsection