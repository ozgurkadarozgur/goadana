<div id="destroy-place-type-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mekan Türünü Sil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Mekan türünü silmek istediğine emin misin?</p>
            </div>
            <div class="modal-footer">
                <form id="destroy-place-type-form" method="post" action="#">
                    @csrf
                    @method('DELETE')
                </form>
                <button type="button" onclick="document.getElementById('destroy-place-type-form').submit();" class="btn btn-outline-primary">Evet</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>