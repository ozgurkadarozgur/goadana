<div id="add-from-existing-places-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $place_type->title }} | Mevcut Mekanlardan Ekle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="add-from-existing-places-form" method="post" action="{{ route('admin.place-type.add-from-existing-places', $place_type->id) }}">
                    @csrf
                    <ul class="list-group">
                        @forelse($the_places_not_exists as $item)
                            <li class="list-group-item">
                                <div class="custom-control custom-checkbox">
                                    <input name="places[{{ $item->id }}]" type="checkbox" class="custom-control-input" id="places{{ $item->id }}">
                                    <label class="custom-control-label" for="places{{ $item->id }}">
                                        {{ $item->title }}
                                        @foreach($item->place_type_matches as $match_item)
                                            <span class="badge badge-success">{{ $match_item->place_type->title }}</span>
                                        @endforeach
                                    </label>
                                </div>
                            </li>
                            @empty
                                <span>Mekan bulunamadı.</span>
                        @endforelse
                    </ul>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="document.getElementById('add-from-existing-places-form').submit();" class="btn btn-outline-primary">Ekle</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>