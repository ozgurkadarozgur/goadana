@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place-type.create') }}

        <h3>Mekan Türü Ekle</h3>

        <hr />

        <form method="post" action="{{ route('admin.place-type.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="title">Resim</label>
                <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">
                <div class="invalid-feedback">
                    {{ $errors->first('image') }}
                </div>
            </div>

            <div class="form-group">
                <label for="title">Mekan Türü</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Türü">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Ekle</button>
        </form>
    </div>

@endsection