<div id="remove-place-from-place-type-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <span id="place-text"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Bu kategoriden kaldırmak istediğine emin misin?</p>
            </div>
            <div class="modal-footer">
                <form id="remove-place-from-place-type-form" method="post" action="{{ route('admin.place-type.remove-place', $place_type->id) }}">
                    @csrf
                    <input type="hidden" id="place-id" name="place_id">
                </form>
                <button type="button" onclick="document.getElementById('remove-place-from-place-type-form').submit();" class="btn btn-outline-primary">Evet</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>