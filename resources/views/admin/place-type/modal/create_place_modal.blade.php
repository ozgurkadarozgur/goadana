<div id="create-place-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $place_type->title }} | Mekan Ekle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-place-form" method="post" action="{{ route('admin.place-type.create-place', $place_type->id) }}">
                    @csrf
                    <div class="form-group">
                        <label for="title">Mekan Adı</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Adı">
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Mekan Açıklaması</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Mekan Açıklaması">
                </textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="document.getElementById('create-place-form').submit();" class="btn btn-outline-primary">Ekle</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>