@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('plan.show', $plan) }}
        @include('admin.plan.modal.confirm_plan_destroy_modal')
        @include('admin.plan.modal.confirm_plan_item_destroy_modal')
        @include('admin.plan.modal.create_plan_item_modal')
        @include('admin.plan.modal.update_plan_photo_modal')
        @include('admin.plan.modal.update_plan_item_photo_modal')
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $plan->title }}</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.plan.edit', $plan->id) }}">Düzenle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-plan-photo-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Değiştir</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#destroy-plan-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#create-plan-item-modal" data-backdrop="static" data-keyboard="false">İçerik Ekle</a>
                    </div>
                </div>

            </div>
        </div>

        <hr />

        <div class="card mb-3">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <div style="min-width: 200px; min-height: 200px; position:relative;">
                        <img src="{{ $plan->image_url }}" style="min-width: 200px; min-height: 200px;" class="card-img img-thumbnail">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">{{ $plan->title }}</h5>
                        <p class="card-text">{!! $plan->text !!}</p>
                        <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $plan->created_at->format('d-m-Y') }}</small></p>
                    </div>
                </div>
            </div>
        </div>

        <hr />

        <span style="font-size: x-large">İçerikler</span>

        @forelse($plan->items as $item)
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ $item->image_url }}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <div class="dropdown float-right">
                                <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ...
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('admin.plan.item.edit', $item->id) }}">Düzenle</a>
                                    <a class="dropdown-item" onclick="setItemToUpdatePhoto({{ $item->id }})" href="#" data-toggle="modal" data-target="#update-plan-item-photo-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Değiştir</a>
                                    <a class="dropdown-item" onclick="setItemToDestroy({{ $item->id }})" href="#" data-toggle="modal" data-target="#destroy-plan-item-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                                </div>
                            </div>
                            <h5 class="card-title">{{ $item->title }}</h5>
                            <p class="card-text">{!! $item->text !!}</p>
                            <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $item->created_at->format('d-m-Y') }}</small></p>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <p>İçerik bulunamadı.</p>
        @endforelse

    </div>

@endsection

@section('scripts')

    <script>

        function setItemToUpdatePhoto(id) {
            let url = ('{{ route('admin.plan.item.update_photo', ':id') }}');
            url = url.replace(':id', id);
            document.getElementById('update-plan-item-photo-form').action = url;
        }

        var map;
        var current_marker = null;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 36.9978517, lng: 35.3220206},
                zoom: 15
            });

            map.addListener('click', function(e) {
                let lat = e.latLng.lat();
                let lng = e.latLng.lng();

                var marker = new google.maps.Marker({
                    position: e.latLng,
                    title:"Hello World!"
                });

                if (current_marker != null) {
                    current_marker.setMap(null)
                }

                marker.setMap(map);
                current_marker = marker;
                document.getElementById('location').value = JSON.stringify({
                    lat: current_marker.position.lat(),
                    lng: current_marker.position.lng()
                });
                console.log(document.getElementById('location').value)
            });

        }

        function setItemToDestroy(id) {
            let route = ('{{ route('admin.plan.item.destroy', ':id') }}');
            route = route.replace(':id', id);
            console.log(route);
            document.getElementById('destroy-plan-item-form').action = route;
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxjqcD2kYEBrF9g24eZzzyofKAl3tdEPo&callback=initMap"
            async defer></script>
@endsection