<div id="create-plan-item-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $plan->title }} | İçerik Ekle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-plan-item-form" method="post" action="{{ route('admin.plan.item.store', $plan->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="image">Resim</label>
                        <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">
                        <div class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">Başlık</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık">
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text">İçerik</label>
                        <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik"></textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('text') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text">Konum</label>
                        <p>
                            <a class="btn btn-outline-primary btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-map-marker"></i>&nbsp;
                                <span>Konum seçin...</span>
                            </a>
                        </p>
                        <input type="hidden" class="form-control @error('location') is-invalid @enderror" id="location" name="location">
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                <div id="map" style="height: 500px;"></div>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="document.getElementById('create-plan-item-form').submit();" class="btn btn-outline-primary">Ekle</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('text');
            CKEDITOR.editorConfig = function( config ) {
                config.toolbarGroups = [
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'others', groups: [ 'others' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'about', groups: [ 'about' ] }
                ];

                config.removeButtons = 'Underline,Subscript,Superscript,Image';
            };
        }, 100);

    </script>

@endsection