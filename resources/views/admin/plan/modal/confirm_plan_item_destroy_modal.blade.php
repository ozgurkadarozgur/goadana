<div id="destroy-plan-item-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Silmek istediğine emin misin?</p>
            </div>
            <div class="modal-footer">
                <form id="destroy-plan-item-form" method="post" action="#">
                    @csrf
                    @method('DELETE')
                </form>
                <button type="button" onclick="document.getElementById('destroy-plan-item-form').submit();" class="btn btn-outline-primary">Evet</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>