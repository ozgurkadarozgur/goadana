@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('plan.index') }}
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">Planla</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.plan.create') }}">Yeni Ekle</a>
                    </div>
                </div>
            </div>
        </div>

        <hr />

        <ul class="list-group">
            @foreach($plans as $item)
                <li class="list-group-item">
                    <b>{{ $loop->iteration . ')' }}</b>
                    {{ $item->title }}

                    <div class="float-right">
                        <a href="{{ route('admin.plan.show', $item->id) }}">İncele</a>
                    </div>
                </li>
            @endforeach
        </ul>

    </div>

@endsection