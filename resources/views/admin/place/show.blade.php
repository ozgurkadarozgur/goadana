@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place.show', $place) }}
        @include('admin.place.modal.confirm_place_destroy_modal')
        @include('admin.place.modal.update_place_photo_modal')
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $place->title }}</span>
                @foreach($place->place_type_matches as $match_item)
                    <span class="badge badge-success">{{ $match_item->place_type->title }}</span>
                @endforeach
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.place.edit', $place->id) }}">Düzenle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-place-photo-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Değiştir</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#destroy-place-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('admin.place.gallery', $place->id) }}">Galeri</a>
                    </div>
                </div>

            </div>
        </div>
        <img src="{{ $place->image_url }}" style="min-width: 200px; min-height: 200px;" class="card-img img-thumbnail">
        <hr />

        <p>{!!  $place->description !!}</p>

        @foreach($place->featureList as $match_item)
            <span class="badge badge-danger">{{ $match_item->title }}</span>
        @endforeach

    </div>

@endsection
