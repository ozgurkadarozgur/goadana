@extends('layouts.app')

@section('content')

    <div class="container">

        @if(session()->has('message'))
            <div class="alert alert-primary" role="alert">
                {{ session()->get('message') }}
            </div>
        @endif

        {{ Breadcrumbs::render('place.gallery', $place) }}
        @include('admin.place.modal.gallery.create_gallery_item_modal')
        @include('admin.place.modal.gallery.confirm_destroy_gallery_item_modal')
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $place->title }}</span>
                @foreach($place->place_type_matches as $match_item)
                    <span class="badge badge-success">{{ $match_item->place_type->title }}</span>
                @endforeach
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#create-gallery-item-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Ekle</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="container">

        <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Galeri</h1>

        <hr class="mt-2 mb-5">

        <div class="row text-center text-lg-left">

            @foreach($place->gallery as $item)
                <div class="card text-center" style="width: 18rem;">
                    <img class="card-img-top" src="{{ $item->image_url }}" alt="Card image cap">
                    <div class="card-footer">
                        <a class="btn btn-sm btn-danger" onclick="setItemToDestroy({{ $item->id }})" href="#" data-toggle="modal" data-target="#destroy-gallery-item-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
    <!-- /.container -->

@endsection

@section('scripts')

    <script>
        function setItemToDestroy(id) {
            let route = ('{{ route('admin.gallery.item.destroy', ':id') }}');
            route = route.replace(':id', id);
            document.getElementById('destroy-gallery-item-form').action = route;
        }
    </script>

@endsection