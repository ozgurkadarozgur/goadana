@extends('layouts.app')

@section('content')

    {{ $errors }}

    <div class="container">
        {{ Breadcrumbs::render('place.edit', $place) }}
        @include('admin.place.modal.select_location_modal')
        <h4>{{ $place->title }}</h4>
        <hr />

        <form method="post" action="{{ route('admin.place.update', $place->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">

                <div class="col-md-6">

                    <div class="form-group">
                        <label for="image">Konum</label>
                        <br />
                        <a class="btn btn-outline-primary btn-sm" href="#" data-toggle="modal" data-target="#select-location-modal" data-backdrop="static" data-keyboard="false">
                            <i class="fa fa-map-marker"></i>&nbsp;
                            <span>Konum seçin...</span>
                        </a>
                        <input type="hidden" class="form-control @error('image') is-invalid @enderror" id="location" name="location">
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title">Mekan Adı</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Adı" value="{{ $place->title }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Mekan Açıklaması</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Mekan Açıklaması">{{ $place->description }}</textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Telefon</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="Telefon" value="{{ $place->phone }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('phone') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Adres</label>
                        <textarea class="form-control @error('address') is-invalid @enderror" id="address" name="address" placeholder="Adres">{{ $place->address }}</textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('address') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="working_hours">Çalışma Saatleri</label>
                        <input type="text" class="form-control @error('working_hours') is-invalid @enderror" id="working_hours" name="working_hours" placeholder="Çalışma Saatleri" value="{{ $place->working_hours }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('working_hours') }}
                        </div>
                    </div>
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Mekan Özellikleri
                                    </button>
                                </h2>
                                <div id="place-feature-container"></div>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <ul class="list-group">
                                        @foreach($place_features as $item)
                                            <li class="list-group-item">
                                                <div class="custom-control custom-checkbox">
                                                    <input {{ ($place->features) ? (in_array($item->id, json_decode($place->features)) ? 'checked' : '') : '' }} name="place_features[{{ $item->id }}]" type="checkbox" onchange="manageFeatureList(this, '{{$item->title}}')" class="custom-control-input" id="place_features{{ $item->id }}">
                                                    <label class="custom-control-label" for="place_features{{ $item->id }}">{{ $item->title }}</label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('description');
        }, 100);


        var map;
        var current_marker = null;
        initFeatures();
        function initMap() {
            let lat = ('{{ $place->latitude }}');
            let lng = ('{{ $place->longitude }}');

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: parseFloat(lat), lng: parseFloat(lng)},
                zoom: 15
            });

            var initialLatLng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
            var initialMarker = new google.maps.Marker({
                position: initialLatLng,
                title:"Hello World!"
            });

            addMarker(initialMarker);

            map.addListener('click', function(e) {
                let lat = e.latLng.lat();
                let lng = e.latLng.lng();

                console.log('latlng', e.latLng)

                var marker = new google.maps.Marker({
                    position: e.latLng,
                    title:"Hello World!"
                });
                addMarker(marker);
            });

        }

        function addMarker(marker) {
            if (current_marker != null) {
                current_marker.setMap(null)
            }

            marker.setMap(map);
            current_marker = marker;
            document.getElementById('location').value = JSON.stringify({
                lat: current_marker.position.lat(),
                lng: current_marker.position.lng()
            });
            console.log(document.getElementById('location').value)
        }

        function initTypes() {
            let place_type_container = document.getElementById('place-type-container');

            let place_types = @json($place_types);
            console.log(place_types);

            let current_type_ids = @json($place->place_type_matches->pluck('place_type_id'));
            console.log(current_type_ids);

            let box_list = place_types.filter(item => current_type_ids.includes(item.id));
            console.log(box_list);

            box_list.forEach(e => {
                place_type_container.innerHTML += '<span class="badge badge-success mr-2" id="i_place_types' + e.id + '" >'+ e.title +'</span>';
            });

        }

        function manageTypeList(e, title) {
            let place_type_container = document.getElementById('place-type-container');
            if (e.checked) {
                console.log(place_type_container.childNodes);
                place_type_container.innerHTML += '<span class="badge badge-success mr-2" id="i_' + e.id + '" >'+ title +'</span>';
            } else {
                document.getElementById('i_'+ e.id).remove();
            }
        }

        function initFeatures() {
            let place_feature_container = document.getElementById('place-feature-container');

            let place_features = @json($place_features);
            console.log(place_features);

            let current_feature_ids = @json($place->features);
            console.log(current_feature_ids);

            let box_list = place_features.filter(item => current_feature_ids.includes(item.id));
            console.log(box_list);

            box_list.forEach(e => {
                place_feature_container.innerHTML += '<span class="badge badge-danger mr-2" id="i_place_features' + e.id + '" >'+ e.title +'</span>';
            });
        }

        function manageFeatureList(e, title) {
            let place_feature_container = document.getElementById('place-feature-container');
            if (e.checked) {
                place_feature_container.innerHTML += '<span class="badge badge-danger mr-2" id="i_' + e.id + '" >'+ title +'</span>';
            } else {
                document.getElementById('i_'+ e.id).remove();
            }
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxjqcD2kYEBrF9g24eZzzyofKAl3tdEPo&callback=initMap"
            async defer></script>

@endsection