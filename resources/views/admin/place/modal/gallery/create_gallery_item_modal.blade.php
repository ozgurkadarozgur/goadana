<div id="create-gallery-item-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $place->title }} | Fotoğraf Ekle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-gallery-item-form" method="post" action="{{ route('admin.gallery.store') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="table" value="places">
                    <input type="hidden" name="owner_id" value="{{ $place->id }}">
                    <div class="form-group">
                        <label for="description">Fotoğraf Seçin</label>
                        <input type="file" name="image" class="form-control">
                        <div class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="document.getElementById('create-gallery-item-form').submit();" class="btn btn-outline-primary">Ekle</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>