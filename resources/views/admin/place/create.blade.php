@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('place.create') }}
        @include('admin.place.modal.select_location_modal')
        <h3>Mekan Ekle</h3>

        <hr />

        <form method="post" action="{{ route('admin.place.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">

                <div class="col-md-6">

                    <div class="form-group">
                        <label for="image">Konum</label>
                        <br />
                        <a class="btn btn-outline-primary btn-sm" href="#" data-toggle="modal" data-target="#select-location-modal" data-backdrop="static" data-keyboard="false">
                            <i class="fa fa-map-marker"></i>&nbsp;
                            <span>Konum seçin...</span>
                        </a>
                        <input type="hidden" class="form-control @error('image') is-invalid @enderror" id="location" name="location">
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image">Resim</label>
                        <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image" >
                        <div class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">Mekan Adı</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Mekan Adı">
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Mekan Açıklaması</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Mekan Açıklaması">
                </textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Telefon</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="Telefon">
                        <div class="invalid-feedback">
                            {{ $errors->first('phone') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Adres</label>
                        <textarea class="form-control @error('address') is-invalid @enderror" id="address" name="address" placeholder="Adres">
                        </textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('address') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="working_hours">Çalışma Saatleri</label>
                        <input type="text" class="form-control @error('working_hours') is-invalid @enderror" id="working_hours" name="working_hours" placeholder="Çalışma Saatleri">
                        <div class="invalid-feedback">
                            {{ $errors->first('working_hours') }}
                        </div>
                    </div>
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Mekan Türleri
                                    </button>
                                </h2>
                                <div id="place-type-container"></div>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <ul class="list-group">
                                        @foreach($place_types as $item)
                                            <li class="list-group-item">
                                                <div class="custom-control custom-checkbox">
                                                    <input name="place_types[{{ $item->id }}]" type="checkbox" onchange="manageTypeList(this, '{{$item->title}}')" class="custom-control-input" id="place_types{{ $item->id }}">
                                                    <label class="custom-control-label" for="place_types{{ $item->id }}">{{ $item->title }}</label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Mekan Özellikleri
                                    </button>
                                </h2>
                                <div id="place-feature-container"></div>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <ul class="list-group">
                                        @foreach($place_features as $item)
                                            <li class="list-group-item">
                                                <div class="custom-control custom-checkbox">
                                                    <input name="place_features[{{ $item->id }}]" type="checkbox" onchange="manageFeatureList(this, '{{$item->title}}')" class="custom-control-input" id="place_features{{ $item->id }}">
                                                    <label class="custom-control-label" for="place_features{{ $item->id }}">{{ $item->title }}</label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Ekle</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>
        setTimeout(function () {
            CKEDITOR.replace('description');
        }, 100);


        var map;
        var current_marker = null;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 36.9978517, lng: 35.3220206},
                zoom: 15
            });

            map.addListener('click', function(e) {
                let lat = e.latLng.lat();
                let lng = e.latLng.lng();

                var marker = new google.maps.Marker({
                    position: e.latLng,
                    title:"Hello World!"
                });

                if (current_marker != null) {
                    current_marker.setMap(null)
                }

                marker.setMap(map);
                current_marker = marker;
                document.getElementById('location').value = JSON.stringify({
                    lat: current_marker.position.lat(),
                    lng: current_marker.position.lng()
                });
                console.log(document.getElementById('location').value)
            });

        }

        function manageTypeList(e, title) {
            let place_type_container = document.getElementById('place-type-container');
            if (e.checked) {
                place_type_container.innerHTML += '<span class="badge badge-success mr-2" id="i_' + e.id + '" >'+ title +'</span>';
            } else {
                document.getElementById('i_'+ e.id).remove();
            }
        }

        function manageFeatureList(e, title) {
            let place_feature_container = document.getElementById('place-feature-container');
            if (e.checked) {
                place_feature_container.innerHTML += '<span class="badge badge-danger mr-2" id="i_' + e.id + '" >'+ title +'</span>';
            } else {
                document.getElementById('i_'+ e.id).remove();
            }
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxjqcD2kYEBrF9g24eZzzyofKAl3tdEPo&callback=initMap"
            async defer></script>
@endsection