@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('discover.create', request('id')) }}

        <h3>Yeni Ekle</h3>

        <hr />

        <form method="post" action="{{ route('admin.discover.store', request('id')) }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="title">Resim</label>
                <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">
                <div class="invalid-feedback">
                    {{ $errors->first('image') }}
                </div>
            </div>

            <div class="form-group">
                <label for="title">Başlık</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık" value="{{ old('title') }}">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>
            <div class="form-group">
                <label for="text">İçerik</label>
                <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik">{{ old('text') }}</textarea>
                <div class="invalid-feedback">
                    {{ $errors->first('text') }}
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Ekle</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('text');
            CKEDITOR.editorConfig = function( config ) {
                config.toolbarGroups = [
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'others', groups: [ 'others' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'about', groups: [ 'about' ] }
                ];

                config.removeButtons = 'Underline,Subscript,Superscript,Image';
            };
        }, 100);

    </script>

@endsection