<div id="create-plan-item-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $discover->title }} | İçerik Ekle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-discover-item-form" method="post" enctype="multipart/form-data" action="{{ route('admin.discover.item.store', $discover->id) }}">
                    @csrf
                    <div class="form-group">
                        <label for="title">Resim</label>
                        <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">
                        <div class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">Video ID</label>
                        <input type="text" class="form-control @error('video_id') is-invalid @enderror" id="video_id" name="video_id" placeholder="Video ID" value="{{ old('video_id') }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('video_id') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title">Başlık</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık">
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text">İçerik</label>
                        <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik"></textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('text') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text">Konum</label>
                        <p>
                            <a class="btn btn-outline-primary btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-map-marker"></i>&nbsp;
                                <span>Konum seçin...</span>
                            </a>
                        </p>
                        <input type="hidden" class="form-control @error('location') is-invalid @enderror" id="location" name="location">
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                <div id="map" style="height: 500px;"></div>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="document.getElementById('create-discover-item-form').submit();" class="btn btn-outline-primary">Ekle</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">İptal</button>
            </div>
        </div>
    </div>
</div>