@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('discover_item.edit', $item) }}
        <h4>{{ $item->title }}</h4>
        <hr />

        <form method="post" action="{{ route('admin.discover.item.update', $item->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Başlık</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık" value="{{ $item->title }}">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>
            <div class="form-group">
                <label for="text">Video ID</label>
                <input class="form-control @error('video_id') is-invalid @enderror" id="video_id" name="video_id" placeholder="Video ID" value="{{ $item->video_id }}" />
                <div class="invalid-feedback">
                    {{ $errors->first('video_id') }}
                </div>
            </div>
            <div class="form-group">
                <label for="text">İçerik</label>
                <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik">{{ $item->text }}</textarea>
                <div class="invalid-feedback">
                    {{ $errors->first('text') }}
                </div>
            </div>
            <div class="form-group">
                <label for="text">Konum</label>
                <p>
                    <a class="btn btn-outline-primary btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-map-marker"></i>&nbsp;
                        <span>Konum seçin...</span>
                    </a>
                </p>
                <input type="hidden" class="form-control @error('image') is-invalid @enderror" id="location" name="location">
                <div class="invalid-feedback">
                    {{ $errors->first('location') }}
                </div>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <div id="map" style="height: 500px;"></div>
                    </div>
                </div>
                <div class="invalid-feedback">
                    {{ $errors->first('location') }}
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('text');
            CKEDITOR.editorConfig = function( config ) {
                config.toolbarGroups = [
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'others', groups: [ 'others' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'about', groups: [ 'about' ] }
                ];

                config.removeButtons = 'Underline,Subscript,Superscript,Image';
            };
        }, 100);

        var map;
        var current_marker = null;
        function initMap() {

            let lat = ('{{ $item->latitude }}');
            let lng = ('{{ $item->longitude }}');

            let center = (lat != '' && lng != '') ? {lat: parseFloat(lat), lng: parseFloat(lng)} : {lat: 36.9978517, lng: 35.3220206};

            map = new google.maps.Map(document.getElementById('map'), {
                center: center,
                zoom: 15
            });

            if (lat != '' && lng != '') {
                var initialLatLng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
                var initialMarker = new google.maps.Marker({
                    position: initialLatLng,
                    title:"Hello World!"
                });

                addMarker(initialMarker);
            }

            map.addListener('click', function(e) {
                let lat = e.latLng.lat();
                let lng = e.latLng.lng();

                var marker = new google.maps.Marker({
                    position: e.latLng,
                    title:"Hello World!"
                });
                addMarker(marker);
            });

        }

        function addMarker(marker) {
            if (current_marker != null) {
                current_marker.setMap(null)
            }

            marker.setMap(map);
            current_marker = marker;
            document.getElementById('location').value = JSON.stringify({
                lat: current_marker.position.lat(),
                lng: current_marker.position.lng()
            });
            console.log(document.getElementById('location').value)
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxjqcD2kYEBrF9g24eZzzyofKAl3tdEPo&callback=initMap"
            async defer></script>

@endsection