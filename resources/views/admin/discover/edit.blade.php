@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('discover.edit', $discover) }}
        <h4>{{ $discover->title }}</h4>
        <hr />

        <form method="post" action="{{ route('admin.discover.update', $discover->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Başlık</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Başlık" value="{{ $discover->title }}">
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
            </div>
            <div class="form-group">
                <label for="text">İçerik</label>
                <textarea class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="İçerik">{{ $discover->text }}</textarea>
                <div class="invalid-feedback">
                    {{ $errors->first('text') }}
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('text');
        }, 100);

    </script>

@endsection