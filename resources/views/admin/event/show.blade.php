@extends('layouts.app')

@section('content')

    @include('admin.event.modal.confirm_event_destroy_modal')
    @include('admin.event.modal.update_event_photo_modal')
    <div class="container">
        {{ Breadcrumbs::render('event.show', $event) }}
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">{{ $event->title }}</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.event.edit', $event->id) }}">Düzenle</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-event-photo-modal" data-backdrop="static" data-keyboard="false">Fotoğraf Değiştir</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#destroy-event-modal" data-backdrop="static" data-keyboard="false">Sil</a>
                    </div>
                </div>

            </div>
        </div>

        <hr />

        <div class="card mb-3">
            <img class="card-img-top" src="{{ $event->image_url }}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{ $event->title }}</h5>
                <p class="card-text">{!! $event->description !!}</p>
                <p class="card-text"><small class="text-muted">Eklenme tarihi: {{ $event->created_at->format('d-m-Y') }}</small></p>
            </div>
        </div>

    </div>

@endsection