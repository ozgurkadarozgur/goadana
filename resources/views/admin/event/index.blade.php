@extends('layouts.app')

@section('content')

    <div class="container">
        {{ Breadcrumbs::render('event.index') }}
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: x-large">Etkinlikler</span>
                <div class="dropdown float-right">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        İşlemler
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.event.create') }}">Yeni Ekle</a>
                    </div>
                </div>
            </div>
        </div>

        <hr />

        <ul class="list-group">
            @foreach($events as $item)
                <li class="list-group-item">
                    <b>{{ $loop->iteration . ')' }}</b>
                    {{ $item->title }}
                    <span class="text-muted">{{ \Carbon\Carbon::parse($item->start_date)->isoFormat('LL') }}</span>
                    <span class="badge badge-success">{{ $item->event_type->title }}</span>
                    <div class="float-right">
                        <a href="{{ route('admin.event.show', $item->id) }}">İncele</a>
                    </div>
                </li>
            @endforeach
        </ul>

    </div>

@endsection