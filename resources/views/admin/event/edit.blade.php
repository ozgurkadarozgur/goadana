@extends('layouts.app')

@section('content')
    {{ $errors }}
    <div class="container">
        {{ Breadcrumbs::render('event.edit', $event) }}
        @include('admin.event.modal.select_location_modal')
        <h3>Etkinlik Düzenle</h3>

        <hr />

        <form method="post" action="{{ route('admin.event.update', $event->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="image">Konum</label>
                        <br />
                        <a class="btn btn-outline-primary btn-sm" href="#" data-toggle="modal" data-target="#select-location-modal" data-backdrop="static" data-keyboard="false">
                            <i class="fa fa-map-marker"></i>&nbsp;
                            <span>Konum seçin...</span>
                        </a>
                        <input type="hidden" class="form-control @error('image') is-invalid @enderror" id="location" name="location">
                        <div class="invalid-feedback">
                            {{ $errors->first('location') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title">Etkinlik Adı</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Etkinlik Adı" value="{{ $event->title }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="start_date">Başlangıç Tarihi</label>
                        <input type="date" class="form-control @error('start_date') is-invalid @enderror" id="start_date" name="start_date" value="{{ $event->start_date }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('start_date') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="start_time">Başlangıç Saati</label>
                        <input type="text" class="form-control @error('start_time') is-invalid @enderror" id="start_time" name="start_time" value="{{ $event->start_time }}">
                        <div class="invalid-feedback">
                            {{ $errors->first('start_time') }}
                        </div>
                    </div>
                    <span style="font-size: medium">Etkinlik Türleri</span>
                    <select class="form-control" name="event_type_id">
                        @foreach($event_types as $item)
                            <option value="{{ $item->id }}" {{ ($item->id == $event->event_type_id) ? 'selected': '' }}>{{ $item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Etkinlik Açıklaması</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Etkinlik Açıklaması">{{ $event->description }}</textarea>
                        <div class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>

        setTimeout(function () {
            CKEDITOR.replace('description');
        }, 100);

        var map;
        var current_marker = null;
        function initMap() {

            let lat = ('{{ $event->latitude }}');
            let lng = ('{{ $event->longitude }}');

            let center = (lat != '' && lng != '') ? {lat: parseFloat(lat), lng: parseFloat(lng)} : {lat: 36.9978517, lng: 35.3220206};

            map = new google.maps.Map(document.getElementById('map'), {
                center: center,
                zoom: 15
            });

            if (lat != '' && lng != '') {
                var initialLatLng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
                var initialMarker = new google.maps.Marker({
                    position: initialLatLng,
                    title:"Hello World!"
                });

                addMarker(initialMarker);
            }

            map.addListener('click', function(e) {
                let lat = e.latLng.lat();
                let lng = e.latLng.lng();

                var marker = new google.maps.Marker({
                    position: e.latLng,
                    title:"Hello World!"
                });

                addMarker(marker);
            });

        }

        function addMarker(marker) {
            if (current_marker != null) {
                current_marker.setMap(null)
            }

            marker.setMap(map);
            current_marker = marker;
            document.getElementById('location').value = JSON.stringify({
                lat: current_marker.position.lat(),
                lng: current_marker.position.lng()
            });
            console.log(document.getElementById('location').value)
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxjqcD2kYEBrF9g24eZzzyofKAl3tdEPo&callback=initMap"
            async defer></script>
@endsection