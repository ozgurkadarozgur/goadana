<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group(['namespace' => 'api\v1', 'prefix' => 'v1'], function () {

    Route::get('/plans', 'PlanController@index');
    Route::get('/plans/{id}', 'PlanController@show');
    Route::get('/plans/{id}/locations', 'PlanController@locations')->name('api.plan.location');


    Route::get('/get-its', 'GetItController@index');

    Route::get('/discover-copies', 'DiscoverController@copies');
    Route::get('/get-discovers/{id?}', 'DiscoverController@index');
    Route::get('/discovers/{id}', 'DiscoverController@show');
    Route::get('/discovers/{id}/locations', 'DiscoverController@locations')->name('api.discover.location');

    Route::get('/event-types', 'EventController@event_types');
    Route::get('/event-types/{event_type_id}/events', 'EventController@index');
    Route::get('/events/{id}', 'EventController@show');
    Route::post('/events/filter-n-sort', 'EventController@filter_n_sort')->name('api.event.filter-n-sort');
    Route::get('/event-locations', 'EventController@locations')->name('api.event.locations');

    Route::get('/place-types', 'PlaceTypeController@index');
    Route::get('/place-types/{id}', 'PlaceTypeController@show');
    Route::get('/place-type-locations', 'PlaceTypeController@locations')->name('api.place_type.locations');

});