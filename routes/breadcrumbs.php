<?php

use \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

$discoverCopyService = app(\App\Services\DiscoverCopyService::class);

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Ana Sayfa', route('home'));
});

Breadcrumbs::for('place.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Mekanlar', route('admin.place.index'));
});

Breadcrumbs::for('place.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Mekanlar', route('admin.place.index'));
    $trail->push('Yeni Ekle', route('admin.place.create'));
});

Breadcrumbs::for('place.show', function ($trail, $place) {
    $trail->parent('home');
    $trail->push('Mekanlar', route('admin.place.index'));
    $trail->push($place->title, route('admin.place.show', $place->id));
});

Breadcrumbs::for('place.gallery', function ($trail, $place) {
    $trail->parent('home');
    $trail->push('Mekanlar', route('admin.place.index'));
    $trail->push($place->title, route('admin.place.show', $place->id));
    $trail->push('Galeri', route('admin.place.gallery', $place->id));
});

Breadcrumbs::for('place.edit', function ($trail, $place) {
    $trail->parent('home');
    $trail->push('Mekanlar', route('admin.place.index'));
    $trail->push($place->title, route('admin.place.show', $place->id));
    $trail->push('Düzenle', route('admin.place.edit', $place->id));
});

Breadcrumbs::for('place-type.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Mekan Türleri', route('admin.place-type.index'));
});

Breadcrumbs::for('place-type.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Mekan Türleri', route('admin.place-type.index'));
    $trail->push('Yeni Ekle', route('admin.place-type.create'));
});

Breadcrumbs::for('place-type.show', function ($trail, $place_type) {
    $trail->parent('home');
    $trail->push('Mekan Türleri', route('admin.place-type.index'));
    $trail->push($place_type->title, route('admin.place-type.show', $place_type->id));
});

Breadcrumbs::for('place-type.edit', function ($trail, $place_type) {
    $trail->parent('home');
    $trail->push('Mekan Türleri', route('admin.place-type.index'));
    $trail->push($place_type->title, route('admin.place-type.show', $place_type->id));
    $trail->push('Düzenle', route('admin.place-type.edit', $place_type->id));
});

Breadcrumbs::for('place-feature.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Mekan Özellikleri', route('admin.place-feature.index'));
});

Breadcrumbs::for('place-feature.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Mekan Özellikleri', route('admin.place-feature.index'));
    $trail->push('Yeni Ekle', route('admin.place-type.create'));
});

Breadcrumbs::for('plan.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Planla', route('admin.plan.index'));
});

Breadcrumbs::for('plan.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Planla', route('admin.plan.index'));
    $trail->push('Yeni Ekle', route('admin.plan.create'));
});

Breadcrumbs::for('plan.show', function ($trail, $plan) {
    $trail->parent('home');
    $trail->push('Planla', route('admin.plan.index'));
    $trail->push($plan->title, route('admin.plan.show', $plan->id));
});

Breadcrumbs::for('plan.edit', function ($trail, $plan) {
    $trail->parent('home');
    $trail->push('Planla', route('admin.plan.index'));
    $trail->push($plan->title, route('admin.plan.show', $plan->id));
    $trail->push('Düzenle', route('admin.plan.edit', $plan->id));
});

Breadcrumbs::for('plan_item.edit', function ($trail, $plan_item) {
    $trail->parent('home');
    $trail->push('Planla', route('admin.plan.index'));
    $trail->push($plan_item->plan->title, route('admin.plan.show', $plan_item->plan->id));
    $trail->push($plan_item->title, route('admin.plan.item.edit', $plan_item->id));
});

Breadcrumbs::for('get-it.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Anla', route('admin.plan.index'));
});

Breadcrumbs::for('get-it.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Anla', route('admin.get-it.index'));
    $trail->push('Yeni Ekle', route('admin.get-it.create'));
});

Breadcrumbs::for('get-it.show', function ($trail, $get_it) {
    $trail->parent('home');
    $trail->push('Anla', route('admin.get-it.index'));
    $trail->push($get_it->title, route('admin.get-it.show', $get_it->id));
});

Breadcrumbs::for('get-it.edit', function ($trail, $get_it) {
    $trail->parent('home');
    $trail->push('Anla', route('admin.get-it.index'));
    $trail->push($get_it->title, route('admin.get-it.show', $get_it->id));
    $trail->push('Düzenle', route('admin.get-it.edit', $get_it->id));
});

Breadcrumbs::for('event.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Etkinlik', route('admin.event.index'));
});

Breadcrumbs::for('event.show', function ($trail, $event) {
    $trail->parent('home');
    $trail->push('Etkinlik', route('admin.event.index'));
    $trail->push($event->title, route('admin.event.show', $event->id));
});

Breadcrumbs::for('event.edit', function ($trail, $event) {
    $trail->parent('home');
    $trail->push('Etkinlik', route('admin.event.index'));
    $trail->push($event->title, route('admin.event.show', $event->id));
    $trail->push('Düzenle', route('admin.event.edit', $event->id));
});

Breadcrumbs::for('event-type.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Etkinlik Türleri', route('admin.event-type.index'));
});

Breadcrumbs::for('event-type.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Etkinlik Türleri', route('admin.event-type.index'));
    $trail->push('Yeni Ekle', route('admin.event-type.create'));
});

Breadcrumbs::for('event-type.show', function ($trail, $event_type) {
    $trail->parent('home');
    $trail->push('Etkinlik Türleri', route('admin.event-type.index'));
    $trail->push($event_type->title, route('admin.event-type.show', $event_type->id));
});

Breadcrumbs::for('event-type.edit', function ($trail, $event_type) {
    $trail->parent('home');
    $trail->push('Etkinlik Türleri', route('admin.event-type.index'));
    $trail->push($event_type->title, route('admin.event-type.show', $event_type->id));
    $trail->push('Düzenle', route('admin.event-type.edit', $event_type->id));
});

Breadcrumbs::for('discover.create', function ($trail, $copy_id) use ($discoverCopyService) {
    $trail->parent('home');
    $trail->push($discoverCopyService->findTitleById($copy_id), route('admin.discover.index', $copy_id));
    $trail->push('Yeni Ekle', route('admin.discover.create', $copy_id));
});

Breadcrumbs::for('discover.index', function ($trail, $copy_id) use ($discoverCopyService) {
    $trail->parent('home');
    $trail->push($discoverCopyService->findTitleById($copy_id), route('admin.discover.index', $copy_id));
});

Breadcrumbs::for('discover.show', function ($trail, $discover) use ($discoverCopyService) {
    $trail->parent('home');
    $trail->push($discoverCopyService->findTitleById($discover->copy_id), route('admin.discover.index', $discover->copy_id));
    $trail->push($discover->title, route('admin.discover.show', $discover->id));
});

Breadcrumbs::for('discover.edit', function ($trail, $discover) {
    $trail->parent('home');
    $trail->push('Keşfet', route('admin.discover.index'));
    $trail->push($discover->title, route('admin.discover.show', $discover->id));
    $trail->push('Düzenle', route('admin.discover.edit', $discover->id));
});

Breadcrumbs::for('discover_item.edit', function ($trail, $discover_item) {
    $trail->parent('home');
    $trail->push('Keşfet', route('admin.plan.index'));
    $trail->push($discover_item->discover->title, route('admin.plan.show', $discover_item->discover->id));
    $trail->push($discover_item->title, route('admin.plan.item.edit', $discover_item->id));
});
