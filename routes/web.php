<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'WebSite', 'domain' => env('APP_ENV') == 'local' ? env('LOCAL_DOMAIN') : env('PROD_DOMAIN')], function (){
/*
    Route::get('/', function () {
        return 'website';
    });*/
    //Route::get('/', 'PublicController@index2');
    Route::get('/index2', 'PublicController@index2');

    Route::get('/', 'PublicController@home');
    Route::get('/planla', 'PublicController@planla')->name('web.planla');
    Route::get('/kesfet/{id?}/{slug?}', 'PublicController@kesfet')->name('web.kesfet');
    Route::get('/afiyetle', 'PublicController@afiyetle')->name('web.afiyetle');
    Route::get('/anla', 'PublicController@anla')->name('web.anla');
    Route::get('/etkinlikler', 'PublicController@etkinlikler')->name('web.etkinlikler');
    Route::get('/harita', 'PublicController@harita')->name('web.harita');


});

require_once "admin_routes.php";

Route::get('/test', function () {
    $a =  new \Illuminate\Support\HtmlString('<p><span style=\"font-size:100px\"><strong>Lorem Ipsum</strong>, dizgi ve baskı end&uuml;strisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak &uuml;zere bir yazı galerisini alarak karıştırdığı 1500&#39;lerden beri end&uuml;stri standardı sahte metinler olarak kullanılmıştır. Beşy&uuml;z yıl boyunca varlığını s&uuml;rd&uuml;rmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sı&ccedil;ramıştır. 1960&#39;larda Lorem Ipsum pasajları da i&ccedil;eren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum s&uuml;r&uuml;mleri i&ccedil;eren masa&uuml;st&uuml; yayıncılık yazılımları ile pop&uuml;ler olmuştur.</span></p>');
});
//Route::get('/map', 'HomeController@map');
