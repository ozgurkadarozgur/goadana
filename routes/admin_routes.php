<?php

Auth::routes(['register'=>false]);

Route::group(['namespace' => 'Admin', 'domain' => (env('APP_ENV') == 'local' ? 'admin.'.env('LOCAL_DOMAIN') : 'admin.'.env('PROD_DOMAIN'))], function (){
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/places', 'PlaceController@index')->name('admin.place.index');
    Route::get('/places/create', 'PlaceController@create')->name('admin.place.create');
    Route::post('/places', 'PlaceController@store')->name('admin.place.store');
    Route::get('/places/{id}', 'PlaceController@show')->name('admin.place.show');
    Route::get('/places/{id}/edit', 'PlaceController@edit')->name('admin.place.edit');
    Route::patch('/places/{id}', 'PlaceController@update')->name('admin.place.update');
    Route::delete('/places/{id}', 'PlaceController@destroy')->name('admin.place.destroy');
    Route::patch('/places/{id}/update-photo', 'PlaceController@update_photo')->name('admin.place.update_photo');
    Route::get('/places/{id}/gallery', 'PlaceController@gallery')->name('admin.place.gallery');


    Route::post('/gallery', 'GalleryController@store')->name('admin.gallery.store');
    Route::delete('/gallery/{id}', 'GalleryController@destroy')->name('admin.gallery.item.destroy');

    Route::get('/place-types', 'PlaceTypeController@index')->name('admin.place-type.index');
    Route::get('/place-types/create', 'PlaceTypeController@create')->name('admin.place-type.create');
    Route::post('/place-types', 'PlaceTypeController@store')->name('admin.place-type.store');
    Route::get('/place-types/{id}', 'PlaceTypeController@show')->name('admin.place-type.show');
    Route::get('/place-types/{id}/edit', 'PlaceTypeController@edit')->name('admin.place-type.edit');
    Route::patch('/place-types/{id}', 'PlaceTypeController@update')->name('admin.place-type.update');
    Route::post('/place-types/{id}/create-place', 'PlaceTypeController@create_place')->name('admin.place-type.create-place');
    Route::post('/place-types/{id}/add-from-existing-places', 'PlaceTypeController@add_from_existing_places')->name('admin.place-type.add-from-existing-places');
    Route::post('/place-types/{id}/remove-place', 'PlaceTypeController@remove_place')->name('admin.place-type.remove-place');
    Route::patch('/place-types/{id}/update-photo', 'PlaceTypeController@update_photo')->name('admin.place-type.update_photo');

    Route::get('/place-features', 'PlaceFeatureController@index')->name('admin.place-feature.index');
    Route::get('/place-features/create', 'PlaceFeatureController@create')->name('admin.place-feature.create');
    Route::post('/place-features', 'PlaceFeatureController@store')->name('admin.place-feature.store');
    Route::get('/place-features/{id}', 'PlaceFeatureController@show')->name('admin.place-feature.show');
    Route::get('/place-features/{id}/edit', 'PlaceFeatureController@edit')->name('admin.place-feature.edit');

    Route::get('/plans', 'PlanController@index')->name('admin.plan.index');
    Route::get('/plans/create', 'PlanController@create')->name('admin.plan.create');
    Route::post('/plans', 'PlanController@store')->name('admin.plan.store');
    Route::get('/plans/{id}', 'PlanController@show')->name('admin.plan.show');
    Route::get('/plans/{id}/edit', 'PlanController@edit')->name('admin.plan.edit');
    Route::patch('/plans/{id}', 'PlanController@update')->name('admin.plan.update');
    Route::delete('/plans/{id}', 'PlanController@destroy')->name('admin.plan.destroy');
    Route::patch('/plans/{id}/update-photo', 'PlanController@update_photo')->name('admin.plan.update_photo');

    Route::post('/plans/{id}/items', 'PlanController@store_item')->name('admin.plan.item.store');
    Route::delete('/plan-item/{id}', 'PlanController@destroy_item')->name('admin.plan.item.destroy');
    Route::get('/plan-item/{id}/edit', 'PlanController@edit_item')->name('admin.plan.item.edit');
    Route::patch('/plan-item/{id}', 'PlanController@update_item')->name('admin.plan.item.update');
    Route::patch('/plan-item/{id}/update-photo', 'PlanController@update_item_photo')->name('admin.plan.item.update_photo');

    Route::get('/get-it', 'GetItController@index')->name('admin.get-it.index');
    Route::get('/get-it/create', 'GetItController@create')->name('admin.get-it.create');
    Route::post('/get-it', 'GetItController@store')->name('admin.get-it.store');
    Route::get('/get-it/{id}', 'GetItController@show')->name('admin.get-it.show');
    Route::get('/get-it/{id}/edit', 'GetItController@edit')->name('admin.get-it.edit');
    Route::patch('/get-it/{id}', 'GetItController@update')->name('admin.get-it.update');
    Route::delete('/get-it/{id}', 'GetItController@destroy')->name('admin.get-it.destroy');
    Route::patch('/get-it/{id}/update-photo', 'GetItController@update_photo')->name('admin.get-it.update_photo');

    Route::get('/event-types', 'EventTypeController@index')->name('admin.event-type.index');
    Route::get('/event-types/create', 'EventTypeController@create')->name('admin.event-type.create');
    Route::post('/event-types', 'EventTypeController@store')->name('admin.event-type.store');
    Route::get('/event-types/{id}', 'EventTypeController@show')->name('admin.event-type.show');
    Route::get('/event-types/{id}/edit', 'EventTypeController@edit')->name('admin.event-type.edit');
    Route::patch('/event-types/{id}', 'EventTypeController@update')->name('admin.event-type.update');
    Route::post('/event-types/{id}/create-event', 'EventTypeController@create_event')->name('admin.event-type.create-event');
    Route::delete('/event-types/{id}', 'EventTypeController@destroy')->name('admin.event-type.destroy');
    Route::patch('/event-types/{id}/update-photo', 'EventTypeController@update_photo')->name('admin.event-type.update_photo');

    Route::get('/events', 'EventController@index')->name('admin.event.index');
    Route::get('/events/create', 'EventController@create')->name('admin.event.create');
    Route::post('/events', 'EventController@store')->name('admin.event.store');
    Route::get('/events/{id}', 'EventController@show')->name('admin.event.show');
    Route::get('/events/{id}/edit', 'EventController@edit')->name('admin.event.edit');
    Route::patch('/events/{id}', 'EventController@update')->name('admin.event.update');
    Route::delete('/events/{id}', 'EventController@destroy')->name('admin.event.destroy');
    Route::patch('/events/{id}/update-photo', 'EventController@update_photo')->name('admin.event.update_photo');

    Route::get('/discovers/{id}/get', 'DiscoverController@index')->name('admin.discover.index');
    Route::get('/discovers/{id}/create', 'DiscoverController@create')->name('admin.discover.create');
    Route::post('/discovers/{id}', 'DiscoverController@store')->name('admin.discover.store');
    Route::get('/discovers/{id}', 'DiscoverController@show')->name('admin.discover.show');
    Route::get('/discovers/{id}/edit', 'DiscoverController@edit')->name('admin.discover.edit');
    Route::patch('/discovers/{id}', 'DiscoverController@update')->name('admin.discover.update');
    Route::delete('/discovers/{id}', 'DiscoverController@destroy')->name('admin.discover.destroy');
    Route::patch('/discovers/{id}/update-photo', 'DiscoverController@update_photo')->name('admin.discover.update_photo');

    Route::post('/discovers/{id}/items', 'DiscoverController@store_item')->name('admin.discover.item.store');
    Route::delete('/discover-item/{id}', 'DiscoverController@destroy_item')->name('admin.discover.item.destroy');
    Route::get('/discover-item/{id}/edit', 'DiscoverController@edit_item')->name('admin.discover.item.edit');
    Route::patch('/discover-item/{id}', 'DiscoverController@update_item')->name('admin.discover.item.update');
    Route::patch('/discover-item/{id}/update-photo', 'DiscoverController@update_item_photo')->name('admin.discover.item.update_photo');

});
