<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceTypeMatch extends Model
{
    protected $table = 'place_type_matches';

    protected $fillable = [
        'place_type_id',
        'place_id',
    ];

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id');
    }

    public function place_type()
    {
        return $this->belongsTo(PlaceType::class, 'place_type_id');
    }

}
