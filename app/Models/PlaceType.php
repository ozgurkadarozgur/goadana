<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PlaceType
 * @package App\Models
 * @property $id
 * @property $title
 * @property $image_url
 */

class PlaceType extends Model
{

    protected $table = 'place_types';

    public function place_matches()
    {
        return $this->hasMany(PlaceTypeMatch::class, 'place_type_id');
    }
}
