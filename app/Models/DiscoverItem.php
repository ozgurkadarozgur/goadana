<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DiscoverItem
 * @package App\Models
 * @property $id
 * @property $discover_id
 * @property $title
 * @property $text
 * @property $image_url
 * @property $video_id
 * @property $latitude
 * @property $longitude
 *
 */

class DiscoverItem extends Model
{
    protected $table = 'discover_items';

    public function discover()
    {
        return $this->belongsTo(Discover::class, 'discover_id');
    }

    public function getLocationAttribute()
    {
        return [
            'image_url' => $this->image_url,
            'title' => $this->title,
            'lat' => $this->latitude,
            'long' => $this->longitude,
        ];
    }

}
