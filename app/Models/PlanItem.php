<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PlanItem
 * @package App\Models
 * @property $id
 * @property $plan_id
 * @property $title
 * @property $text
 * @property $image_url
 * @property $latitude
 * @property $longitude
 */

class PlanItem extends Model
{
    protected $table = 'plan_items';

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function getLocationAttribute()
    {
        return [
            'image_url' => $this->image_url,
            'title' => $this->title,
            'lat' => $this->latitude,
            'long' => $this->longitude,
        ];
    }

}
