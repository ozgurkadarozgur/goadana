<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscoverCopy extends Model
{
    protected $table = 'discover_copies';

    protected $fillable = [
      'title', 'slug', 'icon', 'image_url',
    ];

    public const ITEMS = [
        [
            'title' => 'Kent Merkezi Yürüyüş Rotaları',
            'slug' => 'kent-merkezi-yuruyus-rotalari',
            'icon' => 'icon',
            'image_url' => 'image_url',
        ],
        [
            'title' => 'Müzeler ve Tarihi Yapılar',
            'slug' => 'muzeler-ve-tarihi-yapilar',
            'icon' => 'icon',
            'image_url' => 'image_url',
        ],
        [
            'title' => 'Kent Yaşamı',
            'slug' => 'kent-yasami',
            'icon' => 'icon',
            'image_url' => 'image_url',
        ],
        [
            'title' => 'Parklar ve Yeşil Alanlar',
            'slug' => 'parklar-ve-yesil-alanlar',
            'icon' => 'icon',
            'image_url' => 'image_url',
        ],
        [
            'title' => 'Alışveriş',
            'slug' => 'alisveris',
            'icon' => 'icon',
            'image_url' => 'image_url',
        ],
    ];

}
