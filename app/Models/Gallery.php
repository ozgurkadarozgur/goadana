<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Gallery
 * @package App\Models
 * @property $id
 * @property $table
 * @property $owner_id
 * @property $image_url
 */
class Gallery extends Model
{
    protected $table = 'gallery';
}
