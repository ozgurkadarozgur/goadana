<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Plan
 * @package App\Models
 * @property $id
 * @property $title
 * @property $text
 * @property $image_url
 * @property $created_at
 * @property $updated_at
 */
class Plan extends Model
{
    protected $table = 'plans';

    public function items()
    {
        return $this->hasMany(PlanItem::class, 'plan_id');
    }

}
