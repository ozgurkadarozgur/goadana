<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PlaceFeature
 * @package App\Models
 * @property $id
 * @property $title
 * @property $icon_url
 */
class PlaceFeature extends Model
{
    //
}
