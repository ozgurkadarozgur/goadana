<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Discover
 * @package App\Models
 * @property $id
 * @property $title
 * @property $text
 * @property $image_url
 * @property $copy_id
 */

class Discover extends Model
{
    protected $table = 'discovers';

    public function items()
    {
        return $this->hasMany(DiscoverItem::class, 'discover_id');
    }

}
