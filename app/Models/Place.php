<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Place
 * @package App\Models
 * @property $id
 * @property $title
 * @property $description
 * @property $image_url
 * @property $types
 * @property $features
 * @property $latitude
 * @property $longitude
 * @property $phone
 * @property $address
 * @property $working_hours
 */

class Place extends Model
{
    protected $table = 'places';

    public function place_type_matches()
    {
        //return PlaceType::find(json_decode($this->types));
        return $this->hasMany(PlaceTypeMatch::class, 'place_id');
    }

    public function getFeatureListAttribute()
    {
        return PlaceFeature::find(json_decode($this->features));
        //return $this->hasMany(PlaceFeatureMatch::class, 'place_id');
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class, 'owner_id')
            ->where('table', 'places');
    }

    public function slug_type_matches()
    {
        $slug = '';
        foreach ($this->place_type_matches as $item) {
            $slug .= ' '. Str::slug($item->place_type->title, '-');
        }
        return $slug;
    }

    public function slug_type_matches_css()
    {
        $slug = '';
        foreach ($this->place_type_matches as $item) {
            $slug .= ' .' . Str::slug($item->place_type->title, '-');
        }
        return $slug;
    }

    public function getLocationAttribute()
    {
        return [
            'image_url' => $this->image_url,
            'title' => $this->title,
            'lat' => $this->latitude,
            'long' => $this->longitude,
        ];
    }

}
