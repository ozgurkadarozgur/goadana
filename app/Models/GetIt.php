<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GetIt
 * @package App\Models
 * @property $id
 * @property $title
 * @property $text
 * @property $image_url
 */

class GetIt extends Model
{
    protected $table = 'get_it';
}
