<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EventType
 * @package App\Models
 * @property $id
 * @property $title
 * @property $image_url
 */

class EventType extends Model
{
    protected $table = 'event_types';

    public $timestamps = false;

    public function events()
    {
        return $this->hasMany(Event::class, 'event_type_id');
    }

}
