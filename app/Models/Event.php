<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 * @package App\Models
 * @property $id
 * @property $event_type_id
 * @property $title
 * @property $description
 * @property $image_url
 * @property $latitude
 * @property $longitude
 * @property $start_date
 * @property $start_time
 * @property $end_date
 * @property $end_time
 */

class Event extends Model
{
    protected $table = 'events';

    public function event_type()
    {
        return $this->belongsTo(EventType::class, 'event_type_id');
    }

    public function getLocationAttribute()
    {
        return [
            'image_url' => $this->image_url,
            'title' => $this->title,
            'lat' => $this->latitude,
            'long' => $this->longitude,
        ];
    }

}
