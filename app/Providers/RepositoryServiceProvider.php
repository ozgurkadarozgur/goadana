<?php

namespace App\Providers;

use App\Repositories\DiscoverItemRepository;
use App\Repositories\DiscoverRepository;
use App\Repositories\EventRepository;
use App\Repositories\EventTypeRepository;
use App\Repositories\GalleryRepository;
use App\Repositories\GetItRepository;
use App\Repositories\Interfaces\IDiscoverItemRepository;
use App\Repositories\Interfaces\IDiscoverRepository;
use App\Repositories\Interfaces\IEventRepository;
use App\Repositories\Interfaces\IEventTypeRepository;
use App\Repositories\Interfaces\IGalleryRepository;
use App\Repositories\Interfaces\IGetItRepository;
use App\Repositories\Interfaces\IPlaceFeatureRepository;
use App\Repositories\Interfaces\IPlaceRepository;
use App\Repositories\Interfaces\IPlaceTypeRepository;
use App\Repositories\Interfaces\IPlanItemRepository;
use App\Repositories\Interfaces\IPlanRepository;
use App\Repositories\PlaceFeatureRepository;
use App\Repositories\PlaceRepository;
use App\Repositories\PlaceTypeRepository;
use App\Repositories\PlanItemRepository;
use App\Repositories\PlanRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IPlaceRepository::class,
            PlaceRepository::class
        );

        $this->app->bind(
            IPlaceTypeRepository::class,
            PlaceTypeRepository::class
        );

        $this->app->bind(
            IPlaceFeatureRepository::class,
            PlaceFeatureRepository::class
        );

        $this->app->bind(
            IPlanRepository::class,
            PlanRepository::class
        );

        $this->app->bind(
            IPlanItemRepository::class,
            PlanItemRepository::class
        );

        $this->app->bind(
            IGetItRepository::class,
            GetItRepository::class
        );

        $this->app->bind(
            IEventTypeRepository::class,
            EventTypeRepository::class
        );

        $this->app->bind(
            IEventRepository::class,
            EventRepository::class
        );

        $this->app->bind(
            IDiscoverRepository::class,
            DiscoverRepository::class
        );

        $this->app->bind(
            IDiscoverItemRepository::class,
            DiscoverItemRepository::class
        );

        $this->app->bind(
          IGalleryRepository::class,
          GalleryRepository::class
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
