<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 03.10.2020
 * Time: 16:59
 */

namespace App\Services;


use App\Models\DiscoverCopy;

class DiscoverCopyService
{

    public function items()
    {
        return DiscoverCopy::all();
    }

    public function findTitleById(int $id)
    {
        $copy = DiscoverCopy::find($id);
        if ($copy) {
            return $copy->title;
        }
        return 'Keşfet';
    }

}