<?php

namespace App\Http\Requests\Admin\Place;

use Illuminate\Foundation\Http\FormRequest;

class StorePlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'place_types' => 'required',
            'place_features' => 'required',
            'image' => 'required|image',
            'location' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'working_hours' => 'required',
        ];
    }
}
