<?php

namespace App\Http\Requests\Admin\Event;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_type_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image',
            'location' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
        ];
    }
}
