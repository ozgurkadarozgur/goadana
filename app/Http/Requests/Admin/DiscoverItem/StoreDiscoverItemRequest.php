<?php

namespace App\Http\Requests\Admin\DiscoverItem;

use Illuminate\Foundation\Http\FormRequest;

class StoreDiscoverItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'text' => 'required',
            'image' => 'required|image',
            'video_id' => 'nullable',
            'location' => 'required',
        ];
    }
}
