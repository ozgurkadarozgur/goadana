<?php

namespace App\Http\Controllers\WebSite;

use App\Http\Controllers\Controller;
use App\Models\Discover;
use App\Models\Event;
use App\Models\EventType;
use App\Models\GetIt;
use App\Models\Place;
use App\Models\PlaceType;
use App\Models\Plan;
use App\Repositories\Interfaces\IDiscoverRepository;
use App\Repositories\Interfaces\IEventRepository;
use App\Repositories\Interfaces\IEventTypeRepository;
use App\Repositories\Interfaces\IGetItRepository;
use App\Repositories\Interfaces\IPlaceRepository;
use App\Repositories\Interfaces\IPlaceTypeRepository;
use App\Repositories\Interfaces\IPlanRepository;
use Illuminate\Http\Request;

class PublicController extends Controller
{

    private $planRepository;
    private $discoverRepository;
    private $placeTypeRepository;
    private $placeRepository;
    private $getItRepository;
    private $eventTypeRepository;
    private $eventRepository;


    public function __construct(IPlanRepository $planRepository, IDiscoverRepository $discoverRepository, IPlaceTypeRepository $placeTypeRepository, IPlaceRepository $placeRepository, IGetItRepository $getItRepository, IEventTypeRepository $eventTypeRepository, IEventRepository $eventRepository)
    {
        $this->planRepository = $planRepository;
        $this->discoverRepository = $discoverRepository;
        $this->placeTypeRepository = $placeTypeRepository;
        $this->placeRepository = $placeRepository;
        $this->getItRepository = $getItRepository;
        $this->eventTypeRepository = $eventTypeRepository;
        $this->eventRepository = $eventRepository;
    }

    public function index()
    {
        $plans = $this->planRepository->all();
        $place_types = $this->placeTypeRepository->all();
        $places = $this->placeRepository->all();
        $get_its = $this->getItRepository->all();
        //dd($plans);
        return view('web.index', compact('plans', 'place_types', 'places', 'get_its'));
    }

    public function index2()
    {
        $plans = $this->planRepository->all();
        $discovers = $this->discoverRepository->all();
        $place_types = $this->placeTypeRepository->all();
        $places = $this->placeRepository->all();
        $get_its = $this->getItRepository->all();
        $event_types = $this->eventTypeRepository->all();
        $events  = $this->eventRepository->all();
        return view('web.index2', compact('plans', 'discovers', 'place_types', 'places', 'get_its', 'event_types', 'events'));
    }

    public function home()
    {
        $plans = Plan::all()->take(3);
        $discovers = Discover::all()->take(3);
        $place_types = PlaceType::all()->take(3);
        $get_its = GetIt::all()->take(3);
        $event_types = EventType::all()->take(3);
        return view('web.home', compact('plans', 'discovers', 'place_types', 'get_its', 'event_types'));
    }

    public function planla()
    {
        $plans = $this->planRepository->all();
        return view('web.plan', compact('plans'));
    }

    public function kesfet($id = 0)
    {
        $discovers = $this->discoverRepository->getByCopyId($id);
        return view('web.discover', compact('discovers'));
    }

    public function afiyetle()
    {
        $place_types = $this->placeTypeRepository->all();
        $places = $this->placeRepository->all();
        return view('web.afiyetle', compact('place_types','places'));
    }

    public function anla()
    {
        $get_its = $this->getItRepository->all();
        return view('web.anla', compact('get_its'));
    }

    public function etkinlikler()
    {
        $events = $this->eventRepository->all();
        $event_types = $this->eventTypeRepository->all();
        return view('web.etkinlikler', compact('events', 'event_types'));
    }

    public function harita()
    {
        return view('web.harita');
    }

}
