<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Event\FilterAndSortEventRequest;
use App\Http\Resources\Event\EventResource;
use App\Http\Resources\EventType\EventTypeResource;
use App\Models\Event;
use App\Repositories\Interfaces\IEventRepository;
use App\Repositories\Interfaces\IEventTypeRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EventController extends Controller
{
    private $eventRepository;
    private $eventTypeRepository;

    public function __construct(IEventRepository $eventRepository, IEventTypeRepository $eventTypeRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->eventTypeRepository = $eventTypeRepository;
    }

    public function index($event_type_id)
    {
        $event_type = $this->eventTypeRepository->findById($event_type_id);
        $events = $event_type->events;
        $data = EventResource::collection($events);
        return response()->json([
            'data' => $data
        ]);
    }

    public function show(int $id)
    {
        $event = $this->eventRepository->findById($id);
        $data = new EventResource($event);
        return response()->json([
            'data' => $data
        ]);
    }

    public function event_types()
    {
        $event_types = $this->eventTypeRepository->all();
        $data = EventTypeResource::collection($event_types);
        return response()->json([
            'data' => $data
        ]);
    }

    public function filter_n_sort(FilterAndSortEventRequest $request)
    {
        $validated = $request->validated();
        $result = null;
        if (isset($validated['filter'])) {
            $filter = $validated['filter'];
            if (isset($filter['event_type_id'])){
                $event_type_id = $filter['event_type_id'];
                $result = $this->eventRepository->findByEventTypeId($event_type_id);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'event_type_id property is required in filter object',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            $result = $this->eventRepository->all();
        }

        if (isset($validated['sort'])) {
            $sort = $validated['sort'];
            if (isset($sort['type']) and isset($sort['direction'])) {
                $type = $sort['type'];
                $direction = $sort['direction'];
                $result = $result->sortBy($type, SORT_REGULAR, ($direction == "desc"));
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'type and direction properties are required in sort object',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        if ($result) {

            return response()->json([
                'data' => EventResource::collection($result),
                'status' => 'success',
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'status' => 'error',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function locations()
    {
        $locations = Event::all()->pluck('location');
        return response()->json([
            'data' => $locations
        ]);
    }

}
