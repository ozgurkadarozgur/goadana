<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\PlaceType\PlaceTypeResource;
use App\Models\Place;
use App\Repositories\Interfaces\IPlaceTypeRepository;
use Illuminate\Http\Request;

class PlaceTypeController extends Controller
{

    private $placeTypeRepository;

    public function __construct(IPlaceTypeRepository $repository)
    {
        $this->placeTypeRepository = $repository;
    }

    public function index()
    {
        $place_types = $this->placeTypeRepository->all();
        return response()->json([
            'data' => PlaceTypeResource::collection($place_types),
        ]);
    }

    public function show(int $id)
    {
        $place_type = $this->placeTypeRepository->findById($id);
        return response()->json([
            'data' => new PlaceTypeResource($place_type),
        ]);
    }

    public function locations()
    {
        $places = Place::all()->pluck('location');
        return response()->json([
            'data' => $places
        ]);
    }

}
