<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Discover\DiscoverResource;
use App\Models\DiscoverCopy;
use App\Repositories\Interfaces\IDiscoverRepository;
use Illuminate\Http\Request;

class DiscoverController extends Controller
{

    private $discoverRepository;

    public function __construct(IDiscoverRepository $discoverRepository)
    {
        $this->discoverRepository = $discoverRepository;
    }

    public function copies()
    {
        return response()->json([
           'data' => DiscoverCopy::all()->map(function ($item) {
               return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'image_url' => $item->image_url,
                    'icon' => $item->icon,
               ];
           })
        ]);
    }

    public function index(int $id = 0)
    {
        $discovers = $this->discoverRepository->getByCopyId($id);
        $data = DiscoverResource::collection($discovers);
        return response()->json([
            'data' => $data,
        ]);
    }

    public function show(int $id)
    {
        $discover = $this->discoverRepository->findById($id);
        $data = new DiscoverResource($discover);
        return response()->json([
            'data' => $data,
        ]);
    }

    public function locations($id)
    {
        $discover = $this->discoverRepository->findById($id);
        $locations = $discover->items->pluck('location');
        return response()->json([
           'data' => $locations
        ]);
    }

}
