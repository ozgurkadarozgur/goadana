<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Plan\PlanResource;
use App\Repositories\Interfaces\IPlanRepository;
use Illuminate\Http\Request;

class PlanController extends Controller
{

    private $planRepository;

    public function __construct(IPlanRepository $planRepository)
    {
        $this->planRepository = $planRepository;
    }

    public function index()
    {
        $plans = $this->planRepository->all();

        $data = PlanResource::collection($plans);
        return response()->json([
            'data' => $data
        ]);
    }

    public function show(int $id)
    {
        $plan = $this->planRepository->findById($id);
        $data = new PlanResource($plan);
        return response()->json([
            'data' => $data
        ]);
    }

    public function locations($id)
    {
        $plan = $this->planRepository->findById($id);
        $locations = $plan->items->pluck('location');
        return response()->json([
           'data' => $locations
        ]);
    }

}
