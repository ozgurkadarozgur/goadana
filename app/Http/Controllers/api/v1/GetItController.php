<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\GetIt\GetItResource;
use App\Repositories\Interfaces\IGetItRepository;
use Illuminate\Http\Request;

class GetItController extends Controller
{
    private $getItRepository;

    public function __construct(IGetItRepository $getItRepository)
    {
        $this->getItRepository = $getItRepository;
    }

    public function index()
    {
        $get_its = $this->getItRepository->all();
        $data = GetItResource::collection($get_its);
        return response()->json([
            'data' => $data
        ]);
    }

}
