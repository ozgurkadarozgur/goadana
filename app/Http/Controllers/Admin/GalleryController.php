<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Gallery\StoreGalleryItemRequest;
use App\Repositories\Interfaces\IGalleryRepository;
use Illuminate\Http\Request;

class GalleryController extends Controller
{

    private $galleryRepository;

    public function __construct(IGalleryRepository $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;
    }

    public function store(StoreGalleryItemRequest $request)
    {
        $validated = $request->validated();
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $validated['image_url'] = $upload_result['url'];
            $result = $this->galleryRepository->create($validated);
            if (!$result) {
                return redirect()->back()->with('message', 'db error');
            }
        } else {
            return redirect()->back()->with('message', 'cloudinary error');
        }

        return redirect()->back()->with('message', 'Fotoğraf başarıyla eklendi!');
    }

    public function destroy($id)
    {
        $result = $this->galleryRepository->delete($id);
        if ($result){
            return redirect()->back()->with('message', 'Fotoğraf başarıyla silindi!');
        }
        return redirect()->back()->with('message', 'Bir hata oluştu!');
    }

}
