<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Discover\StoreDiscoverRequest;
use App\Http\Requests\Admin\Discover\UpdateDiscoverPhotoRequest;
use App\Http\Requests\Admin\Discover\UpdateDiscoverRequest;
use App\Http\Requests\Admin\DiscoverItem\StoreDiscoverItemRequest;
use App\Http\Requests\Admin\DiscoverItem\UpdateDiscoverItemPhotoRequest;
use App\Http\Requests\Admin\DiscoverItem\UpdateDiscoverItemRequest;
use App\Repositories\Interfaces\IDiscoverItemRepository;
use App\Repositories\Interfaces\IDiscoverRepository;
use Illuminate\Http\Request;

class DiscoverController extends Controller
{

    private $discoverRepository;
    private $discoverItemRepository;

    public function __construct(IDiscoverRepository $discoverRepository, IDiscoverItemRepository $discoverItemRepository)
    {
        $this->middleware('auth');
        $this->discoverRepository = $discoverRepository;
        $this->discoverItemRepository = $discoverItemRepository;
    }

    /**
     * Display a listing of the resource.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id = 0)
    {
        $discovers = $this->discoverRepository->getByCopyId($id);
        return view('admin.discover.index', compact('discovers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.discover.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreDiscoverRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDiscoverRequest $request, $id)
    {
        $validated = $request->validated();
        $validated['copy_id'] = $id;
        $result = $this->discoverRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->route('admin.discover.index', $id);
    }

    /**
     * @param StoreDiscoverItemRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store_item(StoreDiscoverItemRequest $request, $id)
    {
        $validated = $request->validated();
        $result = $this->discoverItemRepository->create($id, $validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $discover = $this->discoverRepository->findById($id);
        return view('admin.discover.show', compact('discover'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discover = $this->discoverRepository->findById($id);
        return view('admin.discover.edit', compact('discover'));
    }

    public function edit_item($id)
    {
        $item = $this->discoverItemRepository->findById($id);
        return view('admin.discover.edit_item', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreDiscoverRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDiscoverRequest $request, $id)
    {
        $validated = $request->validated();
        $this->discoverRepository->update($id, $validated);
        return redirect()->back();
    }

    public function update_photo(UpdateDiscoverPhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $discover = $this->discoverRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $discover->image_url = $upload_result['url'];
            $discover->save();
        }
        return redirect()->back();
    }

    public function update_item(UpdateDiscoverItemRequest $request, $id)
    {
        $validated = $request->validated();
        $this->discoverItemRepository->update($id, $validated);
        return redirect()->back();
    }

    public function update_item_photo(UpdateDiscoverItemPhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $discover = $this->discoverItemRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $discover->image_url = $upload_result['url'];
            $discover->save();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->discoverRepository->delete($id);
        return redirect()->route('admin.discover.index');
    }

    public function destroy_item($id)
    {
        $this->discoverItemRepository->delete($id);
        return redirect()->back();
    }

}
