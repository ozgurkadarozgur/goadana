<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GetIt\StoreGetItRequest;
use App\Http\Requests\Admin\GetIt\UpdateGetItPhotoRequest;
use App\Http\Requests\Admin\GetIt\UpdateGetItRequest;
use App\Http\Requests\Admin\Plan\UpdatePlanRequest;
use App\Repositories\Interfaces\IGetItRepository;
use Illuminate\Http\Request;

class GetItController extends Controller
{

    private $getItRepository;

    /**
     * GetItController constructor.
     * @param IGetItRepository $getItRepository
     */
    public function __construct(IGetItRepository $getItRepository)
    {
        $this->middleware('auth');
        $this->getItRepository = $getItRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_it_list = $this->getItRepository->all();
        return view('admin.get-it.index', compact('get_it_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.get-it.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreGetItRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGetItRequest $request)
    {
        $validated = $request->validated();
        $result = $this->getItRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->route('admin.get-it.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $get_it = $this->getItRepository->findById($id);
        return view('admin.get-it.show', compact('get_it'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_it = $this->getItRepository->findById($id);
        return view('admin.get-it.edit', compact('get_it'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateGetItRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlanRequest $request, $id)
    {
        $validated = $request->validated();
        $this->getItRepository->update($id, $validated);
        return redirect()->back();
    }

    public function update_photo(UpdateGetItPhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $get_it = $this->getItRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $get_it->image_url = $upload_result['url'];
            $get_it->save();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getItRepository->delete($id);
        return redirect()->route('admin.get-it.index');
    }
}
