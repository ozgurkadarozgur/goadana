<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Plan\StorePlanRequest;
use App\Http\Requests\Admin\Plan\UpdatePlanPhotoRequest;
use App\Http\Requests\Admin\Plan\UpdatePlanRequest;
use App\Http\Requests\Admin\PlanItem\StorePlanItemRequest;
use App\Http\Requests\Admin\PlanItem\UpdatePlanItemPhotoRequest;
use App\Http\Requests\Admin\PlanItem\UpdatePlanItemRequest;
use App\Repositories\Interfaces\IPlanItemRepository;
use App\Repositories\Interfaces\IPlanRepository;

class PlanController extends Controller
{

    private $planRepository;
    private $planItemRepository;

    public function __construct(IPlanRepository $planRepository, IPlanItemRepository $planItemRepository)
    {
        $this->middleware('auth');
        $this->planRepository = $planRepository;
        $this->planItemRepository = $planItemRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = $this->planRepository->all();
        return view('admin.plan.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.plan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePlanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlanRequest $request)
    {
        $validated = $request->validated();
        $result = $this->planRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->route('admin.plan.index');
    }

    /**
     * @param StorePlanItemRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store_item(StorePlanItemRequest $request, $id)
    {
        $validated = $request->validated();
        $result = $this->planItemRepository->create($id, $validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plan = $this->planRepository->findById($id);
        return view('admin.plan.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = $this->planRepository->findById($id);
        return view('admin.plan.edit', compact('plan'));
    }

    public function edit_item($id)
    {
        $item = $this->planItemRepository->findById($id);
        return view('admin.plan.edit_item', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePlanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlanRequest $request, $id)
    {
        $validated = $request->validated();
        $this->planRepository->update($id, $validated);
        return redirect()->back();
    }

    public function update_photo(UpdatePlanPhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $plan = $this->planRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $plan->image_url = $upload_result['url'];
            $plan->save();
        }
        return redirect()->back();
    }

    public function update_item(UpdatePlanItemRequest $request, $id)
    {
        $validated = $request->validated();
        $this->planItemRepository->update($id, $validated);
        return redirect()->back();
    }

    public function update_item_photo(UpdatePlanItemPhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $plan = $this->planItemRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $plan->image_url = $upload_result['url'];
            $plan->save();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->planRepository->delete($id);
        return redirect()->route('admin.plan.index');
    }

    public function destroy_item($id)
    {
        $this->planItemRepository->delete($id);
        return redirect()->back();
    }

}
