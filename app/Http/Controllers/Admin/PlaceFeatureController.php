<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PlaceFeature\StorePlaceFeatureRequest;
use App\Repositories\Interfaces\IPlaceFeatureRepository;
use Illuminate\Http\Request;

class PlaceFeatureController extends Controller
{

    private $placeFeatureRepository;

    /**
     * PlaceFeatureController constructor.
     * @param IPlaceFeatureRepository $repository
     */
    public function __construct(IPlaceFeatureRepository $repository)
    {
        $this->middleware('auth');
        $this->placeFeatureRepository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $place_features = $this->placeFeatureRepository->all();
        return view('admin.place-feature.index', compact('place_features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.place-feature.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePlaceFeatureRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlaceFeatureRequest $request)
    {
        $validated = $request->validated();
        $result = $this->placeFeatureRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->icon_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
