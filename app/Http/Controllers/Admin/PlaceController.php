<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Place\StorePlaceRequest;
use App\Http\Requests\Admin\Place\UpdatePlacePhotoRequest;
use App\Http\Requests\Admin\Place\UpdatePlaceRequest;
use App\Repositories\Interfaces\IPlaceFeatureRepository;
use App\Repositories\Interfaces\IPlaceRepository;
use App\Repositories\Interfaces\IPlaceTypeRepository;

class PlaceController extends Controller
{

    private $placeRepository;
    private $placeTypeRepository;
    private $placeFeatureRepository;

    public function __construct(IPlaceRepository $placeRepository, IPlaceTypeRepository $placeTypeRepository, IPlaceFeatureRepository $placeFeatureRepository)
    {
        $this->middleware('auth');
        $this->placeRepository = $placeRepository;
        $this->placeTypeRepository = $placeTypeRepository;
        $this->placeFeatureRepository = $placeFeatureRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = $this->placeRepository->all();
        return view('admin.place.index', compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $place_types = $this->placeTypeRepository->all();
        $place_features = $this->placeFeatureRepository->all();
        return view('admin.place.create', compact('place_types', 'place_features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePlaceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlaceRequest $request)
    {
        $validated = $request->validated();
        $validated['place_types'] = array_keys($validated['place_types']);
        $validated['place_features'] = array_keys($validated['place_features']);
        $result = $this->placeRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->route('admin.place.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place = $this->placeRepository->findById($id);
        return view('admin.place.show', compact('place'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place = $this->placeRepository->findById($id);
        $place_types = $this->placeTypeRepository->all();
        $place_features = $this->placeFeatureRepository->all();
        return view('admin.place.edit', compact('place', 'place_types', 'place_features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePlaceRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlaceRequest $request, $id)
    {
        $validated = $request->validated();
        $validated['place_features'] = array_keys($validated['place_features']);
        //dd($validated);
        $this->placeRepository->update($id, $validated);
        return redirect()->route('admin.place.show', $id);
    }

    public function update_photo(UpdatePlacePhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $discover = $this->placeRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $discover->image_url = $upload_result['url'];
            $discover->save();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->placeRepository->delete($id);
        return redirect()->route('admin.place.index');
    }

    public function gallery($id)
    {
        $place = $this->placeRepository->findById($id);
        return view('admin.place.gallery', compact('place'));
    }

}
