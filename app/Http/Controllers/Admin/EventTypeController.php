<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventType\CreateEventForEventTypeRequest;
use App\Http\Requests\Admin\EventType\StoreEventTypeRequest;
use App\Http\Requests\Admin\EventType\UpdateEventTypePhotoRequest;
use App\Http\Requests\Admin\EventType\UpdateEventTypeRequest;
use App\Repositories\Interfaces\IEventTypeRepository;
use Illuminate\Http\Request;

class EventTypeController extends Controller
{

    private $eventTypeRepository;

    public function __construct(IEventTypeRepository $eventTypeRepository)
    {
        $this->middleware('auth');
        $this->eventTypeRepository = $eventTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event_types = $this->eventTypeRepository->all();
        return view('admin.event-type.index', compact('event_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreEventTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventTypeRequest $request)
    {
        $validated = $request->validated();
        $result = $this->eventTypeRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->route('admin.event-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event_type = $this->eventTypeRepository->findById($id);
        return view('admin.event-type.show', compact('event_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event_type = $this->eventTypeRepository->findById($id);
        return view('admin.event-type.edit', compact('event_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateEventTypeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventTypeRequest $request, $id)
    {
        $validated = $request->validated();
        $this->eventTypeRepository->update($id, $validated);
        return redirect()->back();
    }

    public function update_photo(UpdateEventTypePhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $event_type = $this->eventTypeRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $event_type->image_url = $upload_result['url'];
            $event_type->save();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->eventTypeRepository->delete($id);
        return redirect()->route('admin.event-type.index');
    }

    public function create_event(CreateEventForEventTypeRequest $request, $id)
    {
        $validated = $request->validated();
        $this->eventTypeRepository->createEvent($id, $validated);
        return redirect()->back();
    }

}
