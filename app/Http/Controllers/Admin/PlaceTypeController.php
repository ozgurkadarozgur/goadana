<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CloudinaryHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PlaceType\AddFromExistingPlacesRequest;
use App\Http\Requests\Admin\PlaceType\CreatePlaceForPlaceTypeRequest;
use App\Http\Requests\Admin\PlaceType\CreatePlaceRequest;
use App\Http\Requests\Admin\PlaceType\RemovePlaceFromPlaceTypeRequest;
use App\Http\Requests\Admin\PlaceType\StorePlaceTypeRequest;
use App\Http\Requests\Admin\PlaceType\UpdatePlaceTypePhotoRequest;
use App\Http\Requests\Admin\PlaceType\UpdatePlaceTypeRequest;
use App\Repositories\Interfaces\IPlaceTypeRepository;
use Illuminate\Http\Request;

class PlaceTypeController extends Controller
{

    private $placeTypeRepository;

    public function __construct(IPlaceTypeRepository $placeTypeRepository)
    {
        $this->middleware('auth');
        $this->placeTypeRepository = $placeTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $place_types = $this->placeTypeRepository->all();
        return view('admin.place-type.index', compact('place_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.place-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePlaceTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlaceTypeRequest $request)
    {
        $validated = $request->validated();
        $result = $this->placeTypeRepository->create($validated);
        if ($result) {
            $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
            if ($upload_result) {
                $result->image_url = $upload_result['url'];
                $result->save();
            }
        }
        return redirect()->route('admin.place-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place_type = $this->placeTypeRepository->findById($id);
        $the_places_not_exists = $this->placeTypeRepository->fetchThePlacesNotExists($id);
        return view('admin.place-type.show', compact('place_type', 'the_places_not_exists'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place_type = $this->placeTypeRepository->findById($id);
        return view('admin.place-type.edit', compact('place_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePlaceTypeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlaceTypeRequest $request, $id)
    {
        $validated = $request->validated();
        $this->placeTypeRepository->update($id, $validated);
        return redirect()->route('admin.place-type.show', $id);
    }

    public function update_photo(UpdatePlaceTypePhotoRequest $request, $id)
    {
        $validated = $request->validated();
        $discover = $this->placeTypeRepository->findById($id);
        $upload_result = CloudinaryHelper::upload_image($validated['image'], 'assets');
        if ($upload_result) {
            $discover->image_url = $upload_result['url'];
            $discover->save();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param CreatePlaceForPlaceTypeRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create_place(CreatePlaceForPlaceTypeRequest $request, $id)
    {
        $validated = $request->validated();
        $this->placeTypeRepository->createPlace($id, $validated);
        return redirect()->back();
    }

    /**
     * @param AddFromExistingPlacesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add_from_existing_places(AddFromExistingPlacesRequest $request, $id)
    {
        $validated = $request->validated();
        $validated['places'] = array_keys($validated['places']);
        $this->placeTypeRepository->addFromExistingPlaces($id, $validated);
        return redirect()->back();
    }

    /**
     * @param RemovePlaceFromPlaceTypeRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove_place(RemovePlaceFromPlaceTypeRequest $request, $id)
    {
        $validated = $request->validated();
        $this->placeTypeRepository->removePlace($id, $validated['place_id']);
        return redirect()->back();
    }

}
