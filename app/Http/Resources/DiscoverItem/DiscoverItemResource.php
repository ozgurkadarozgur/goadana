<?php

namespace App\Http\Resources\DiscoverItem;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscoverItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'image_url' => $this->image_url,
            'video_id' => $this->video_id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'created_at' => $this->created_at
        ];
    }
}
