<?php

namespace App\Http\Resources\Plan;

use App\Http\Resources\PlanItem\PlanItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'image_url' => $this->image_url,
            'items' => PlanItemResource::collection($this->items),
        ];
    }
}
