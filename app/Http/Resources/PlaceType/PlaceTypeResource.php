<?php

namespace App\Http\Resources\PlaceType;

use App\Http\Resources\PlaceTypeMatch\PlaceTypeMatchResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaceTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image_url' => $this->image_url,
            'items' => $this->when(request('id'), PlaceTypeMatchResource::collection($this->place_matches))
        ];
    }
}
