<?php

namespace App\Http\Resources\Discover;

use App\Http\Resources\DiscoverItem\DiscoverItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscoverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'image_url' => $this->image_url,
            'items' => DiscoverItemResource::collection($this->items),
        ];
    }
}
