<?php

namespace App\Http\Resources\PlaceFeature;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaceFeatureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'icon_url' => $this->icon_url,
        ];
    }
}
