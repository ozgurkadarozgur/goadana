<?php

namespace App\Http\Resources\PlaceTypeMatch;

use App\Http\Resources\Gallery\GalleryResource;
use App\Http\Resources\PlaceFeature\PlaceFeatureResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaceTypeMatchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->place->id,
            'title' => $this->place->title,
            'text' => $this->place->description,
            'features' => PlaceFeatureResource::collection($this->place->featureList),
            'image_url' => $this->place->image_url,
            'latitude' => $this->place->latitude,
            'longitude' => $this->place->longitude,
            'phone' => $this->place->phone,
            'address' => $this->place->address,
            'working_hours' => $this->place->working_hours,
            'gallery' => GalleryResource::collection($this->place->gallery),
        ];
    }
}
