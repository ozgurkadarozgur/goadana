<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 03.02.2020
 * Time: 19:08
 */

namespace App\Helpers;


use JD\Cloudder\Facades\Cloudder;

class CloudinaryHelper
{
    /**
     * @param $file
     * @param string $folder
     * @return mixed
     */
    public static function upload_image($file, $folder = 'assets')
    {
        Cloudder::upload($file, null, [
            'folder' => $folder,
            'fetch_format' => 'auto',
        ]);
        return Cloudder::getResult();
    }
}