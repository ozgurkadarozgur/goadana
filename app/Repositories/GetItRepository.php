<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 15.01.2020
 * Time: 13:34
 */

namespace App\Repositories;


use App\Models\GetIt;
use App\Repositories\Interfaces\IGetItRepository;
use Illuminate\Support\Collection;

class GetItRepository implements IGetItRepository
{

    public function findById(int $id): ?GetIt
    {
        try {
            $get_it = GetIt::findOrFail($id);
            return $get_it;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function all(): Collection
    {
        try {
            $get_it_list = GetIt::all();
            return $get_it_list;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create($data): ?GetIt
    {
        try {
            $get_it = new GetIt();
            $get_it->title = $data['title'];
            $get_it->text = $data['text'];
            $get_it->save();
            return $get_it;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update($id, $data): ?GetIt
    {
        try {
            $get_it = $this->findById($id);
            $get_it->title = $data['title'];
            $get_it->text = $data['text'];
            $get_it->save();
            return $get_it;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete($id): ?GetIt
    {
        try {
            $get_it = $this->findById($id);
            $get_it->delete();
            return $get_it;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}