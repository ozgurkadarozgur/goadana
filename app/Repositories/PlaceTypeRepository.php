<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 12.01.2020
 * Time: 00:08
 */

namespace App\Repositories;


use App\Models\Place;
use App\Models\PlaceType;
use App\Repositories\Interfaces\IPlaceTypeRepository;
use Illuminate\Support\Collection;

class PlaceTypeRepository implements IPlaceTypeRepository
{

    public function findById(int $id): PlaceType
    {
        $place_type = PlaceType::find($id);
        return $place_type;
    }

    public function all(): Collection
    {
        try {
            $place_types = PlaceType::all();;
            return $place_types;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function create($data): PlaceType
    {
        try {
            $place_type = new PlaceType();
            $place_type->title = $data['title'];
            $place_type->save();
            return $place_type;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function update(int $id, $data): PlaceType
    {
        try {
            $place_type = $this->findById($id);
            $place_type->title = $data['title'];
            $place_type->save();
            return $place_type;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function delete(int $id): PlaceType
    {
        // TODO: Implement delete() method.
    }

    public function fetchThePlacesNotExists(int $id): Collection
    {
        $place_type = $this->findById($id);
        $place_repository = new PlaceRepository();
        $the_places_in_type = $place_type->place_matches()->pluck('place_id');
        $places_not_exists = $place_repository->all()->whereNotIn('id', $the_places_in_type);
        return $places_not_exists;
    }

    public function createPlace(int $id, $data) : ?Place
    {
        $place_repository = new PlaceRepository();
        $data['place_types'] = [$id];
        return $place_repository->create($data);
    }

    public function addFromExistingPlaces($id, $data)
    {
        $place_type = $this->findById($id);
        foreach ($data['places'] as $item){
            $place_type->place_matches()->create([
                'place_id' => $item
            ]);
        }
    }

    public function removePlace($id, $place_id)
    {
        $place_type = $this->findById($id);
        $place_type->place_matches()->where('place_id', $place_id)->delete();
    }

}