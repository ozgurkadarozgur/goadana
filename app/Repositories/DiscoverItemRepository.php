<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 23.01.2020
 * Time: 14:44
 */

namespace App\Repositories;


use App\Models\DiscoverItem;
use App\Repositories\Interfaces\IDiscoverItemRepository;

class DiscoverItemRepository implements IDiscoverItemRepository
{

    public function findById(int $id): ?DiscoverItem
    {
        try {
            $item = DiscoverItem::findOrFail($id);
            return $item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create(int $discover_id, $data): ?DiscoverItem
    {
        try {
            $item = new DiscoverItem();
            $location = json_decode($data['location']);
            $item->discover_id = $discover_id;
            $item->title = $data['title'];
            $item->text = $data['text'];
            $item->video_id = $data['video_id'];
            $item->latitude = $location->lat;
            $item->longitude = $location->lng;
            $item->save();
            return $item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data): ?DiscoverItem
    {
        try {
            $location = json_decode($data['location']);
            $item = $this->findById($id);
            $item->title = $data['title'];
            $item->text = $data['text'];
            $item->video_id = $data['video_id'];
            $item->latitude = $location->lat;
            $item->longitude = $location->lng;
            $item->save();
            return $item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete(int $id)
    {
        try {
            $item = $this->findById($id);
            $item->delete();
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}