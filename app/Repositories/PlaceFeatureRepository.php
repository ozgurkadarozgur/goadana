<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 10.02.2020
 * Time: 15:01
 */

namespace App\Repositories;


use App\Models\PlaceFeature;
use App\Repositories\Interfaces\IPlaceFeatureRepository;
use Illuminate\Support\Collection;

class PlaceFeatureRepository implements IPlaceFeatureRepository
{

    public function findById(int $id): ?PlaceFeature
    {
        try {
            $place_feature = PlaceFeature::findOrFail($id);
            return $place_feature;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function all(): Collection
    {
        try {
            $place_features = PlaceFeature::all();
            return $place_features;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create($data): ?PlaceFeature
    {
        try {
            $place_feature = new PlaceFeature();
            $place_feature->title = $data['title'];
            $place_feature->save();
            return $place_feature;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data): ?PlaceFeature
    {
        try {
            $place_feature = $this->findById($id);
            $place_feature->title = $data['title'];
            $place_feature->save();
            return $place_feature;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete(int $id): ?PlaceFeature
    {
        try {
            $place_feature = $this->findById($id);
            $place_feature->delete();
            return $place_feature;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}