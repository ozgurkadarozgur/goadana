<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 14.01.2020
 * Time: 15:44
 */

namespace App\Repositories;


use App\Models\Plan;
use App\Models\PlanItem;
use App\Repositories\Interfaces\IPlanRepository;
use Illuminate\Support\Collection;

class PlanRepository implements IPlanRepository
{

    public function findById(int $id) : ?Plan
    {
        try {
            $plan = Plan::findOrFail($id);
            return $plan;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function all() : Collection
    {
        return Plan::all();
    }

    public function create($data): ?Plan
    {
        try{
            $plan = new Plan();
            $plan->title = $data['title'];
            $plan->text = $data['text'];
            $plan->save();
            return $plan;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data): ?Plan
    {
        try {
            $plan = $this->findById($id);
            $plan->title = $data['title'];
            $plan->text = $data['text'];
            $plan->save();
            return $plan;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete($id): ?Plan
    {
        try {
            $plan = $this->findById($id);
            $plan->delete();
            return $plan;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

}