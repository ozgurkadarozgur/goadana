<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 23.01.2020
 * Time: 13:57
 */

namespace App\Repositories;


use App\Models\Discover;
use App\Repositories\Interfaces\IDiscoverRepository;
use Illuminate\Support\Collection;

class DiscoverRepository implements IDiscoverRepository
{

    public function findById(int $id): ?Discover
    {
        try {
            $discover = Discover::findOrFail($id);
            return $discover;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function all(): ?Collection
    {
        try {
            $discovers = Discover::all();
            return $discovers;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create($data): ?Discover
    {
        try {
            $discover = new Discover();
            $discover->title = $data['title'];
            $discover->text = $data['text'];
            $discover->copy_id = $data['copy_id'];
            $discover->save();
            return $discover;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data): ?Discover
    {
        try {
            $discover = $this->findById($id);
            $discover->title = $data['title'];
            $discover->text = $data['text'];
            $discover->save();
            return $discover;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete(int $id): ?Discover
    {
        try {
            $discover = $this->findById($id);
            $discover->delete();
            return $discover;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function getByCopyId(int $id): ?Collection
    {
        try {
            $discovers = Discover::where('copy_id', $id)->get();
            return $discovers;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}