<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 10.01.2020
 * Time: 16:02
 */

namespace App\Repositories\Interfaces;


use App\Models\Place;
use Illuminate\Support\Collection;

interface IPlaceRepository
{
    public function findById(int $id) : Place;

    public function all() : Collection;

    public function create($data) : ?Place;

    public function update(int $id, $data) : Place;

    public function delete(int $id) : Place;

}