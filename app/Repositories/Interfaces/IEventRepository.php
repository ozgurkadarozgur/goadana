<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 16.01.2020
 * Time: 14:26
 */

namespace App\Repositories\Interfaces;


use App\Models\Event;
use Illuminate\Support\Collection;

interface IEventRepository
{
    public function findById(int $id) : ?Event;

    public function findByEventTypeId(int $id) : Collection;

    public function all() : Collection;

    public function create($data) : ?Event;

    public function update(int $id, $data) : ?Event;

    public function delete(int $id) : ?Event;
}