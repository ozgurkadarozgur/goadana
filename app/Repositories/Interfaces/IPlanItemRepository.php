<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 14.01.2020
 * Time: 18:42
 */

namespace App\Repositories\Interfaces;


use App\Models\PlanItem;

interface IPlanItemRepository
{
    public function findById(int $id) : ?PlanItem;

    public function create(int $plan_id, $data) : ?PlanItem;

    public function update(int $id, $data) : ?PlanItem;

    public function delete(int $id) : ?PlanItem;

}