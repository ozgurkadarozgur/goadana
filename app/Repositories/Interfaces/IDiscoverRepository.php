<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 23.01.2020
 * Time: 13:56
 */

namespace App\Repositories\Interfaces;


use App\Models\Discover;
use Illuminate\Support\Collection;

interface IDiscoverRepository
{
    public function findById(int $id) : ?Discover;

    public function getByCopyId(int $id): ?Collection;

    public function all() : ?Collection;

    public function create($data) : ?Discover;

    public function update(int $id, $data) : ?Discover;

    public function delete(int $id) : ?Discover;
}