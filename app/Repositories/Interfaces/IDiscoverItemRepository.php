<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 23.01.2020
 * Time: 14:43
 */

namespace App\Repositories\Interfaces;


use App\Models\DiscoverItem;

interface IDiscoverItemRepository
{
    public function findById(int $id) : ?DiscoverItem;

    public function create(int $discover_id, $data) : ?DiscoverItem;

    public function update(int $id, $data) : ?DiscoverItem;

    public function delete(int $id);
}