<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 10.02.2020
 * Time: 14:59
 */

namespace App\Repositories\Interfaces;


use App\Models\PlaceFeature;
use Illuminate\Support\Collection;

interface IPlaceFeatureRepository
{
    public function findById(int $id) : ?PlaceFeature;

    public function all() : Collection;

    public function create($data) : ?PlaceFeature;

    public function update(int $id, $data) : ?PlaceFeature;

    public function delete(int $id) : ?PlaceFeature;
}