<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 14.01.2020
 * Time: 15:44
 */

namespace App\Repositories\Interfaces;


use App\Models\Plan;
use App\Models\PlanItem;
use Illuminate\Support\Collection;

interface IPlanRepository
{

    public function findById(int $id) : ?Plan;

    public function all() : Collection;

    public function create($data) : ?Plan;

    public function update(int $id, $data) : ?Plan;

    public function delete($id) : ?Plan;

}