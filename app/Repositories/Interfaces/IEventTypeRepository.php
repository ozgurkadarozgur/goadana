<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 15.01.2020
 * Time: 15:26
 */

namespace App\Repositories\Interfaces;


use App\Models\Event;
use App\Models\EventType;
use Illuminate\Support\Collection;

interface IEventTypeRepository
{
    public function findById(int $id) : ?EventType;

    public function all() : Collection;

    public function create($data) : ?EventType;

    public function update($id, $data) : ?EventType;

    public function delete($id) : ?EventType;

    public function createEvent($id, $data) : ?Event;
}