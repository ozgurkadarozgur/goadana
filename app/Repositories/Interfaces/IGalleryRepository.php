<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 06.04.2020
 * Time: 18:06
 */

namespace App\Repositories\Interfaces;


use App\Models\Gallery;
use Illuminate\Support\Collection;

interface IGalleryRepository
{
    public function findById(int $id) : ?Gallery;

    public function findByOwnerId(int $id) : Collection;

    public function create(array $data) : ?Gallery;

    public function delete(int $id) : ?Gallery;
}