<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 12.01.2020
 * Time: 00:07
 */

namespace App\Repositories\Interfaces;


use App\Models\PlaceType;
use Illuminate\Support\Collection;

interface IPlaceTypeRepository
{
    public function findById(int $id) : PlaceType;

    public function all() : Collection;

    public function create($data) : PlaceType;

    public function update(int $id, $data) : PlaceType;

    public function delete(int $id) : PlaceType;

    public function fetchThePlacesNotExists(int $id) : Collection;

    public function createPlace(int $id, $data);

    public function addFromExistingPlaces(int $id, $data);

    public function removePlace($id, $place_id);

}