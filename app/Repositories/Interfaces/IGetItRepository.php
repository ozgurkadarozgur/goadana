<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 15.01.2020
 * Time: 13:33
 */

namespace App\Repositories\Interfaces;


use App\Models\GetIt;
use Illuminate\Support\Collection;

interface IGetItRepository
{

    public function findById(int $id) : ?GetIt;

    public function all() : Collection;

    public function create($data) : ?GetIt;

    public function update($id, $data) : ?GetIt;

    public function delete($id) : ?GetIt;

}