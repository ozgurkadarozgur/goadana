<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 15.01.2020
 * Time: 15:28
 */

namespace App\Repositories;

use App\Models\Event;
use App\Models\EventType;
use App\Repositories\Interfaces\IEventTypeRepository;
use Illuminate\Support\Collection;

class EventTypeRepository implements IEventTypeRepository
{

    public function findById(int $id): ?EventType
    {
        try {
            $event_type = EventType::findOrFail($id);
            return $event_type;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function all(): Collection
    {
        try {
            $event_types = EventType::all();
            return $event_types;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create($data): ?EventType
    {
        try {
            $event_type = new EventType();
            $event_type->title = $data['title'];
            $event_type->save();
            return $event_type;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update($id, $data): ?EventType
    {
        try {
            $event_type = $this->findById($id);
            $event_type->title = $data['title'];
            $event_type->save();
            return $event_type;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete($id): ?EventType
    {
        try {
            $event_type = $this->findById($id);
            $event_type->delete();
            return $event_type;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function createEvent($id, $data): ?Event
    {
        $eventRepository = new EventRepository();
        $data['event_type_id'] = $id;
        return $eventRepository->create($data);
    }

}