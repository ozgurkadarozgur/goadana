<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 10.01.2020
 * Time: 16:01
 */

namespace App\Repositories;


use App\Models\Place;
use App\Repositories\Interfaces\IPlaceRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlaceRepository implements IPlaceRepository
{

    public function findById(int $id) : Place
    {
        try {
            $place = Place::findOrFail($id);
            return $place;
        } catch (\Exception $ex) {
            dd($ex);
            return null;
        }
    }

    public function all() : Collection
    {
        try {
            $places = Place::all();
            return $places;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function create($data) : ?Place
    {
        DB::beginTransaction();
        try {
            $location = json_decode($data['location']);
            $place = new Place();
            $place->title = $data['title'];
            $place->description = $data['description'];
            $place->features = json_encode($data['place_features']);
            $place->latitude = $location->lat;
            $place->longitude = $location->lng;
            $place->phone = $data['phone'];
            $place->address = $data['address'];
            $place->working_hours = $data['working_hours'];
            $place->save();


             $place_type_matches = $data['place_types'];

            foreach ($place_type_matches as $item) {
                $place->place_type_matches()->create([
                    'place_type_id' => $item
                ]);
            }
            /*
            $place_feature_matches = $data['place_features'];

            foreach ($place_feature_matches as $item) {
                $place->place_feature_matches()->create([
                    'place_feature_id' => $item
                ]);
            }
             */

            DB::commit();
            return $place;
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data) : Place
    {
        try {
            $place = $this->findById($id);
            $location = json_decode($data['location']);
            $place->title = $data['title'];
            $place->description = $data['description'];
            $place->features = json_encode($data['place_features']);
            $place->latitude = $location->lat;
            $place->longitude = $location->lng;
            $place->phone = $data['phone'];
            $place->address = $data['address'];
            $place->working_hours = $data['working_hours'];
            $place->save();
            return $place;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function delete(int $id) : Place
    {
        try {
            $place = $this->findById($id);
            $place->delete();
            return $place;
        } catch (\Exception $ex) {
            return null;
        }

    }

}