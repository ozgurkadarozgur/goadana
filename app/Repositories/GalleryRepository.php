<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 06.04.2020
 * Time: 18:07
 */

namespace App\Repositories;


use App\Models\Gallery;
use App\Repositories\Interfaces\IGalleryRepository;
use Illuminate\Support\Collection;

class GalleryRepository implements IGalleryRepository
{

    public function findById(int $id): ?Gallery
    {
        try {
            $gallery = Gallery::findOrFail($id);
            return $gallery;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function findByOwnerId(int $id): Collection
    {
        try {
            $gallery = Gallery::where('owner_id', $id)->get();
            return $gallery;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create(array $data): ?Gallery
    {
        try {
            $gallery = new Gallery();
            $gallery->table = $data['table'];
            $gallery->owner_id = $data['owner_id'];
            $gallery->image_url = $data['image_url'];
            $gallery->save();
            return $gallery;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete(int $id): ?Gallery
    {
        try {
            $gallery = $this->findById($id);
            $gallery->delete();
            return $gallery;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}