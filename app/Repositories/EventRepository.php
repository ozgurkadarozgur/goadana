<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 16.01.2020
 * Time: 14:26
 */

namespace App\Repositories;

use App\Models\Event;
use App\Repositories\Interfaces\IEventRepository;
use Illuminate\Support\Collection;

class EventRepository implements IEventRepository
{

    public function findById(int $id): ?Event
    {
        try {
            $event = Event::findOrFail($id);
            return $event;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function findByEventTypeId(int $id): Collection
    {
        try {
            $events = Event::where('event_type_id', $id)->get();
            return $events;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function all(): Collection
    {
        try {
            $events = Event::all();
            return $events;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create($data): ?Event
    {
        try {
            $location = json_decode($data['location']);
            $event = new Event();
            $event->event_type_id = $data['event_type_id'];
            $event->title = $data['title'];
            $event->description = $data['description'];
            $event->latitude = $location->lat;
            $event->longitude = $location->lng;
            $event->start_date = $data['start_date'];
            $event->start_time = array_key_exists('start_time', $data) ? $data['start_time'] : null;
            $event->end_date = array_key_exists('end_date', $data) ? $data['end_date'] : null;
            $event->end_time = array_key_exists('end_time', $data) ? $data['end_time'] : null;
            $event->save();
            return $event;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data): ?Event
    {
        try {
            $location = json_decode($data['location']);
            $event = $this->findById($id);
            $event->event_type_id = $data['event_type_id'];
            $event->title = $data['title'];
            $event->description = $data['description'];
            $event->latitude = $location->lat;
            $event->longitude = $location->lng;
            $event->start_date = $data['start_date'];
            $event->start_time = $data['start_time'];
            $event->save();
            return $event;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete(int $id): ?Event
    {
        try {
            $event = $this->findById($id);
            $event->delete();
            return $event;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}