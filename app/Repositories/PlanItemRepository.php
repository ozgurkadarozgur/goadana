<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 14.01.2020
 * Time: 18:44
 */

namespace App\Repositories;


use App\Models\PlanItem;
use App\Repositories\Interfaces\IPlanItemRepository;

class PlanItemRepository implements IPlanItemRepository
{

    public function findById(int $id): ?PlanItem
    {
        try {
            $plan_item = PlanItem::findOrFail($id);
            return $plan_item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function create(int $plan_id, $data): ?PlanItem
    {
        try {
            $item = new PlanItem();
            $location = json_decode($data['location']);
            $item->plan_id = $plan_id;
            $item->title = $data['title'];
            $item->text = $data['text'];
            $item->latitude = $location->lat;
            $item->longitude = $location->lng;
            /*
            $item->image_url = 'url';
            $item->latitude = 'lat';
            $item->longitude = 'long';
             */
            $item->save();
            return $item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function update(int $id, $data): ?PlanItem
    {
        try {
            $location = json_decode($data['location']);
            $item = $this->findById($id);
            $item->title = $data['title'];
            $item->text = $data['text'];
            $item->latitude = $location->lat;
            $item->longitude = $location->lng;
            $item->save();
            return $item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }

    public function delete(int $id): ?PlanItem
    {
        try {
            $item = $this->findById($id);
            $item->delete();
            return $item;
        } catch (\Exception $ex) {
            if (env('APP_DEBUG')) dd($ex);
            return null;
        }
    }
}